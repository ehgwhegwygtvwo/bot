<?php
error_reporting(0);

//server
//define('ROOT', "");
define('ROOT', $_SERVER['DOCUMENT_ROOT']."/bot");

//database
//87.236.22.18
//define('DB_HOST', 'localhost');
//define('DB_USERNAME', 'God');
//define('DB_PASSWORD', 'godness');
//define('DB_NAME', 'deb');
//localhost
define('DB_HOST', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_NAME', 'deb');

//types
define('MOBIDEL_BOT_TYPE', 1);

//prototypes
/**
 * index - index of current command in stack commands
 * copy_index - saved index when using procedures
 * read - name of reading state variable
 * read_type - type of reading parameter 0 - number 1 - string
 * choice - index of select choice in switch
 * phone, age, family, street, home, room, comment - user personality data
 * index_page_menu - index of page in menu products
 * basket - string of coma separated mobidel id products
 * basket_parameter - parameter that passed buttons in cart
*/
$_PROTOTYPES = [
    'mobidel' => [
        'index' =>[
            'default' => 0,
            'type' => 'integer'
        ],
        'copy_index' =>[
            'default' => 0,
            'type' => 'integer'
        ],
        'read' =>[
            'default' => '',
            'type' => 'string'
        ],
        'choice' =>[
            'default' => -1,
            'type' => 'integer'
        ],
        'continue' => [
            'default' => 0,
            'type' => 'integer'
        ],
        'input' => [
            'default' => '',
            'type' => 'string'
        ],
        'input_correct' => [
            'default' => 1,
            'type' => 'integer'
        ],
        //register
        'deviceID' => [
            'default' => '',
            'type' => 'string'
        ],
        'login' => [
            'default' => '79634827999',
            'type' => 'string'
        ],
        'password' => [
            'default' => '123456',
            'type' => 'string'
        ],

        'sessionID' => [
            'default' => '',
            'type' => 'string'
        ],

        //order data
        'phone' => [
            'default' => '79634827999',
            'type' => 'string'
        ],
        'street' => [
            'default' => '',
            'type' => 'string'
        ],
        'home' => [
            'default' => '',
            'type' => 'integer'
        ],
        'entrance' => [
            'default' => '',
            'type' => 'string'
        ],
        'room' => [
            'default' => '',
            'type' => 'string'
        ],
        'flat' => [
            'default' => '',
            'type' => 'string'
        ],
        'comment' => [
            'default' => '',
            'type' => 'string'
        ],
        'independently' => [ // delivery flag
            'default' => -1,
            'type' => 'integer'
        ],
        'warehouseID' => [ // order building point
            'default' => '',
            'type' => 'string'
        ],
        'bonusPay' => [
            'default' => 0,
            'type' => 'integer'
        ],
        'index_of_make_order_page' => [
            'default' => 1,
            'type' => 'integer'
        ],
        'paymentMethod' => [
            'default' => -1,
            'type' => 'integer'
        ],
        'order_init_index' => [
            'default' => -1,
            'type' => 'integer'
        ],
        'order_init_max_index' => [
            'default' => -1,
            'type' => 'integer'
        ],
        'order_init_name_read_parameter' => [
            'default' => '',
            'type' => 'string'
        ],

        //menu data
        'index_of_category' =>[
            'default' => 1,
            'type' => 'integer'
        ],
        'index_of_page_menu' =>[
            'default' => 1,
            'type' => 'integer'
        ],
        'max_page_menu' => [
            'default' => 0,
            'type' => 'integer'
        ],
        //length of long_next and long_prev
        'long_step' =>[
            'default' => 2,
            'type' => 'integer'
        ],
        'cart' =>[
            'default' => '',
            'type' => 'string'
        ],

//    product
        'type_of_selected_product' => [
            'default' => '',
            'type' => 'integer'
        ],
        'id_selected_product' => [
            'default' => '',
            'type' => 'string'
        ],
        'id_selected_semiproduct' => [
            'default' => '',
            'type' => 'string'
        ],
        'index_item' => [
            'default' => '',
            'type' => 'integer'
        ],
        'menu_parameter' => [
            'default' => 0,
            'type' => 'integer'
        ],
//       price_parameters_list have JSON
        'map_price_parameters' =>[
            'default' => [
                'id' => [],
                'name' => [],
                'configuration' => []
            ],
            'type' => 'array'
        ],
    ]
];

//vk api
define('СALLBACK_API_EVENT_CONFIRMATION', "confirmation");
define('СALLBACK_API_EVENT_MESSAGE_NEW', 'message_new');
//vk api test
define('СALLBACK_API_EVENT_MESSAGE_NEW_TEST', 'message_new_test');

//translator
define('DIR_LOGICAL_COMPONENTS', ROOT.'/src/translator/components');