<?php

namespace TinyException;

/**
 * errors will have six number(three couple) in error code
 *
 * first couple is number of programm section database, api, executor etc.
 *
 * second couple is subsection (ex. api have five files, then error in api will be have
 * code error api in first couple, and code api section in second couple). Code 00 for
 * second couple is always common section error
 *
 * third couple is number of error
 * all error constants have prefix ERR
 *
 * ex. 0101 - in module 01(common errors) error 01(unknown error)
*/
class TinyException extends \Exception {
    // 01 COMMON
    //common errors
    const ERR_UNKNOWN_ERROR = 010001;

    // 02 DB
    // db group
    const ERR_DB_GROUP_ALREADY_EXISTS = 020101;

    // 03  API
    // 00 common
    const ERR_API_UNEXPECTED_ACTION = 030001;
    const ERR_API_NO_ACTION = 030002;
    const ERR_API_PARAMETERS_FAIL = 030003;
    // 01 owner
    const ERR_API_UNKNOWN_OWNER = 030101;
    const ERR_API_OWNER_ALREADY_EXISTS = 030102;
    // 02 bot
    const ERR_API_USER_IS_NOT_OWNER = 030201;

    const MESSAGE = [
        self::ERR_UNKNOWN_ERROR => 'unknown error',
        self::ERR_DB_GROUP_ALREADY_EXISTS => 'group already exists',
        self::ERR_API_UNEXPECTED_ACTION => 'unexpected index',
        self::ERR_API_NO_ACTION => 'action not passed',
        self::ERR_API_PARAMETERS_FAIL => 'parameters fail',
        self::ERR_API_UNKNOWN_OWNER  => 'owner not found',
        self::ERR_API_USER_IS_NOT_OWNER => 'user is not owner',
        self::ERR_API_OWNER_ALREADY_EXISTS => 'owner already exists'
    ];

    public function __construct($code, $previous = null)
    {
        parent::__construct(self::MESSAGE[$code], $code, $previous);
    }
}