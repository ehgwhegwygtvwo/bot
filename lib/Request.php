<?php

class Request
{
    private static $curl = null;

    private static function req_get($options)
    {
        if (!isset($options['url'])) return false;
        if (!self::$curl = curl_init()) return false;

        $url = $options['url'].'?'.http_build_query($options['params']);
        curl_setopt(self::$curl, CURLOPT_URL, $url);
        curl_setopt(self::$curl, CURLOPT_RETURNTRANSFER, true);
        if (isset($options['cookie'])) {
            curl_setopt(self::$curl, CURLOPT_COOKIE, $options['cookie']);
        }
        if (isset($options['curlopt'])) {
            for ($i=0; $i < count($options['curlopt']); $i++) {
                $option = $options['curlopt'][$i];
                curl_setopt(self::$curl, $option['const'], $option['value']);
            }
        }
        $out = curl_exec(self::$curl);
        curl_close(self::$curl);
        return $out;
    }

    private static function req_post($options)
    {
        if (!isset($options['url'])) return false;
        if (!self::$curl = curl_init()) return false;

        $url = $options['url'];
        curl_setopt(self::$curl, CURLOPT_URL, $url);
        curl_setopt(self::$curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(self::$curl, CURLOPT_POST, true);
        if (isset($options['params'])) {
            curl_setopt(self::$curl, CURLOPT_POSTFIELDS, $options['params']);
        }
        if (isset($options['cookie'])) {
            curl_setopt(self::$curl, CURLOPT_COOKIE, $options['cookie']);
        }
        if (isset($options['curlopt'])) {
            for ($i=0; $i < count($options['curlopt']); $i++) {
                $option = $options['curlopt'][$i];
                curl_setopt(self::$curl, $option['const'], $option['value']);
            }
        }
        $out = curl_exec(self::$curl);
        if($out === false) {
            $error = curl_error(self::$curl);
            echo 'Curl error: ' .$error;
        }
        curl_close(self::$curl);
        return $out;
    }

    /**
     * EXAMPLE USING:
     *
     *  [
     *      'method' => 'get',
     *      'url' => 'http://...',
     *      'params' => [
     *          'action' => 'getApplicationData',
     *          'appID' => 'appID',
     *          'appKey' => 'appKey',
     *      ],
     *      'curlopt' => [
     *          [
     *              'const' => CURLOPT_RETURNTRANSFER,
     *              'value' => 1
     *          ]
     *      ]
     *  ]
     * @param $options
     * @return bool|mixed|null
     */
    public static function req($options)
    {
        $result = null;
        switch (strtoupper($options['method'])){
            case 'GET':
                $result = self::req_get($options);
                break;
            case 'POST':
                $result = self::req_post($options);
                break;
            default:
                $result = self::req_get($options);
                break;
        }
        return $result;
    }
}