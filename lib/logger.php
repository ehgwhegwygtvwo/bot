<?php 
/**
 * Creates and writes log files
 */
class Logger {
    /**
     * @var string 	$PATH 		local path where save logs
     * @var array 	$logggers 	list of logs
     */
    public static $PATH = 'log';
    protected static $loggers=array();

    /**
     * @var string 	$name 		label of log, using as file name if not defined file
     * @var string 	$file 		name of a log file
     * @var object 	$fp 		file descriptor
     */
    protected $name;
    protected $file;
    protected $fp;

    public function __construct($name, $file=null){
        $this->name=$name;
        $this->file=$file;

        $this->open();
    }
    public function open(){
        if(self::$PATH===null){
            return false;
        }
        $this->fp=fopen($this->file==null ? self::$PATH.'/'.$this->name.'.log' : self::$PATH.'/'.$this->file,'a+');
    }
    /**
     * Get log by name
     *
     * get log by label(name) if log not exists then create this
     *
     * @param string 	$name 	log name (label)
     * @param string  	$file 	name of a log file
     *
     * @return string instance name of the class
     */
    public static function getLogger($name='root',$file=null){
        if(!isset(self::$loggers[$name])){
            self::$loggers[$name]=new Logger($name, $file);
        }

        return self::$loggers[$name];
    }
    /**
     * Write message to log
     *
     * @param string $message content of a note
     *
     * @return void
     */
    public function log($message){
        if(!is_string($message)){
            $this->logPrint($message);
            return false;
        }

        $log = '['.date('D M d H:i:s Y',time()).'] ';

        if(func_num_args()>1){
            $params=func_get_args();
            $message=call_user_func_array('sprintf', $params);
        }

        $log.=$message;
        $log.="\n";

        $this->_write($log);
    }
    /**
     * Write message to log, if log don't exist create it
     *
     * @param string 	$name 		log name (label)
     * @param string 	$message 	content of a note
     */
    public function log_to($name, $message){
        $log = self::getLogger($name);
        $params=func_get_args();
        array_shift($params);//remove $name
        call_user_func_array(array($log, 'log'), $params);
    }
    /**
     * Using fo write to log file arrays and objects
     */
    private function logPrint($obj){
        ob_start();

        print_r($obj);

        $ob=ob_get_clean();
        $this->log($ob);
    }
    protected function _write($string){
         if (flock($this->fp, LOCK_EX)) {
            fwrite($this->fp, $string);
            flock($this->fp, LOCK_UN);
        }
        // echo $string;
    }
    public function __destruct(){
        fclose($this->fp);
    }
}