> VK API 5.80
# Event message new #
{
   "type": "message_new",
   "object": {
      "date": 1536700674,
      "from_id": 501309167,
      "id": 54,
      "out": 0,
      "peer_id": 501309167, 
      "text": "цук",
      "conversation_message_id": 30,
      "fwd_messages": [],
      "important": false,
      "random_id": 0,
      "attachments": [],
      "is_hidden": false
   },
   "group_id": 170319081
}
# Event message new with keyboard #
{
    "type": "message_new",
    "object": {
        "date": 1536700347,
        "from_id": 501309167,
        "id": 53,
        "out": 0,
        "peer_id": 501309167,
        "text": "Текст кнопки",
        "conversation_message_id": 29,
        "fwd_messages": [],
        "important": false,
        "random_id": 0,
        "attachments": [],
        "payload": "{\"button\":\"15\"}",
        "is_hidden": false
    },
    "group_id": 170319081
}
# Event confirmation #
{
    "type":"confirmation",
    "group_id":170319081
}
# Success send message #
{
    "response":34
}
# Error send message #
{
    "error": {
        "error_code": 901,
        "error_msg": "Can't send messages for users without permission",
        "request_params": [
            {
                "key": "oauth",
                "value": "1"
            },
            {
                "key": "method",
                "value": "messages.send"
            },
            {
                "key": "v",
                "value": "5.80"
            },
            {
                "key": "user_id",
                "value": "444"
            },
            {
                "key": "random_id",
                "value": "43900523"
            }
        ]
    }
}