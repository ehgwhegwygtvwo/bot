<?php

namespace executor;
/**
 * This class read commands while don't meet breakpoint
 * TODO: This class will be on similar assembler module
 */
class Reader
{
    const READ_LIMIT = 50;
    const NUMBER_OF_COMMAND = [
        '100' => 'WRITE',
        '101' => 'READ',
        //jump commands
        '200' => 'JMP',
        '201' => 'JE',
        '202' => 'JN',
        '203' => 'JNE',
        '204' => 'JL',
        '205' => 'JLE',
        '206' => 'JG',
        '207' => 'JGE',
        '208' => 'PROC',
        //assistant commands
        '300' => 'EXEC',
        '301' => 'MOV',
        '302' => 'STOP',
        '303' => 'CMP',
        '304' => 'ENDP',
        '305' => 'SUB',
        '306' => 'ADD',
        '307' => 'PUSH',
        '308' => 'POP'
    ];
    private static
        $CF = 0,
        $ZF = 0,
        $SF = 0,
        $OF = 0,
        $AX = '';
    /**
     * @var integer $mc_read Counter of commands that was read
     */
    private static $mc_readed = 0;
    /**
     * @var bool $f_breakpoint flag stop of reading
     */
    private static $f_breakpoint = false;
    /**
     * @var bool $move flag for condition jumps, determine is necessary of the jump
     */
    private static $move = false;
    /**
     * @var array $actions list of an actions who have to be executed
     */
    private static $actions = [];

    private static $commands = null;
    /**
     * @var array $command current command
     */
    private static $command = null;
    private static $labels = null;
    /**
     * @var string|null $parameter message from vk user for bot function if this was required else null
     */
    private static $parameter = null;
    /**
     * @var \db\State $state list of an actions who have to be executed
     */
    private static $state = null;
    /**
     * @var array $stack
     */
    private static $stack = [];
    /**
     * @param $arg string
     * @return string integer
     */
    private static function get_argument($arg)
    {
        if (isset(self::$state[$arg])) {
            $value = self::$state[$arg];
        } elseif ($arg == 'AX') {
            $value = self::$AX;
        } else {
            $value = $arg;
        }
        return $value;
    }
    /**
     * get values arguments
     */
    private static function get_arguments()
    {
        $arg1 = self::get_argument(self::$command['args'][0]);
        $arg2 = self::get_argument(self::$command['args'][1]);
        return ['arg1' => $arg1, 'arg2' => $arg2];
    }

    private static function read()
    {
        $prop = self::$command['args'][0];
        self::$state['read'] = $prop;
        self::$f_breakpoint = true;
    }

    private static function compare_string()
    {

    }

    private static function write()
    {
        $text = self::$command['args'][0];
        array_push(self::$actions, [
            "code" => 1,
            "arg" => $text,
        ]);
    }

    private static function JMP()
    {
        self::$move = true;
        $label = self::$command['args'][0];
    }

    private static function JE()
    {
        if (self::$ZF === 1) {
            self::$move = true;
            $label = self::$command['args'][0];
        } else {
            self::$move = false;
        }
    }

    private static function JNE()
    {
        if (self::$ZF === 0) {
            self::$move = true;
            $label = self::$command['args'][0];
        } else {
            self::$move = false;
        }
    }

    private static function JL()
    {
        if (self::$SF !== self::$OF) {
            self::$move = true;
            $label = self::$command['args'][0];
        } else {
            self::$move = false;
        }
    }

    private static function JLE()
    {
        if ((self::$ZF === 1) || (self::$SF !== self::$OF)) {
            self::$move = true;
            $label = self::$command['args'][0];
        } else {
            self::$move = false;
        }
    }

    private static function JG()
    {
        if ((self::$ZF === 0) && (self::$SF === self::$OF)) {
            self::$move = true;
            $label = self::$command['args'][0];
        } else {
            self::$move = false;
        }
    }

    private static function JGE()
    {
        if (self::$SF === self::$OF) {
            self::$move = true;
            $label = self::$command['args'][0];
        } else {
            self::$move = false;
        }
    }

    private static function ADD()
    {
        $args = self::get_arguments();
        $result = (int)$args['arg1'] + (int)$args['arg2'];
        $key = self::$command['args'][0];
        if ($key == 'AX') {
            self::$AX = $result;
        } elseif (isset(self::$state[$key])) {
            self::$state[$key] = $result;
        }
    }

    private static function SUB()
    {
        $args = self::get_arguments();
        $result = (int)$args['arg1'] - (int)$args['arg2'];
        $key = self::$command['args'][0];
        if ($key == 'AX') {
            self::$AX = $result;
        } elseif (isset(self::$state[$key])) {
            self::$state[$key] = $result;
        }
    }

    private static function MOV()
    {
        $args = self::get_arguments(); //need only second argument
        $key = self::$command['args'][0];
        if ($key == 'AX') {
            self::$AX = $args['arg2'];
        } elseif (isset(self::$state[$key])) {
            self::$state[$key] = $args['arg2'];
        }
    }

    private static function proc()
    {
        $label = self::$command['args'][0];
        self::$state['copy_index'] = self::$state['index'];
    }

    private static function stop()
    {
        $text = self::$command['args'][0];
        array_push(self::$actions, [
            "code" => 1,
            "arg" => $text,
        ]);
        self::$f_breakpoint = true;
    }

    private static function exec()
    {
        $func_name = self::$command['args'][0];
        array_push(self::$actions, [
            "code" => 2,
            "arg" => $func_name,
        ]);
    }

    private static function endp()
    {
        self::$state['index'] = self::$state['copy_index'];
    }

    private static function CMP()
    {
        $args = self::get_arguments();
        $result = (int)$args['arg1'] - (int)$args['arg2'];
        if ($result == 0) {
            self::$ZF = 1;
            self::$SF = 0;
        } elseif ($result < 0) {
            self::$ZF = 0;
            self::$SF = 1;
        } elseif ($result > 0) {
            self::$ZF = 0;
            self::$SF = 0;
        }
    }

    private static function PUSH()
    {
        $value = self::get_argument(self::$command['args'][0]);
        if (count(self::$state['history']) > 10) {
            array_shift(self::$state['history']);
        }
        array_push(self::$state['history'], $value);

    }

    private static function POP()
    {
        $key = self::$command['args'][0];
        $value = array_pop(self::$state['history']);
        if ($key == 'AX') {
            self::$AX = $value;
        } else {
            self::$state[$key] = $value;
        }
    }

    private static function move_index()
    {
        $code = self::$command['code'];
        if (($code >= 200) && ($code < 300) && (self::$move == true)) {//команды перехода
            $label = self::$command['args'][0];
            if (!self::$labels[$label]) {
                throw new \Exception("Read error - unknown label '$label'");
            }
            self::$state['index'] = self::$labels[$label];
        } else {
            self::$state['index'] += 1;
        }
    }

    /**
     * if the user had to write parameter return true else false
     * @return bool
     */
    private static function parameter_waiting()
    {
        if (self::$state['read'] != '') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * read user message as value of state by key of value by read key
     * @return bool
     */
    private static function read_parameter($parameter)
    {
        $name = self::$state['read'];
        self::$parameter = $parameter;
        self::$state[$name] = $parameter;
        self::$state['read'] = '';
    }

    /**
     * @param string $parameter
     * @param array $commands
     * @param array $labels
     * @param \db\State $state
     * @return array
     * @throws \Exception
     */
    public static function _read($parameter, $commands, $labels, &$state)
    {
        self::$commands = $commands;
        self::$labels = $labels;
        self::$state = &$state;
        self::$f_breakpoint = false;

        if (self::parameter_waiting()) {
            self::read_parameter($parameter);
        }
        while (!self::$f_breakpoint) {
            if (self::$mc_readed > self::READ_LIMIT) {
                throw new \Exception('Read error - dead lock');
            }
            $index = self::$state['index'];
            if ($index === null) {
                throw new \Exception('Read error - index can\'t be null');
            }
            self::$command = self::$commands[$index];
            $func_name = self::NUMBER_OF_COMMAND[self::$command['code']];
            call_user_func(['self', $func_name]);
            self::move_index();
            self::$mc_readed++;
        }
        return self::$actions;
    }
}