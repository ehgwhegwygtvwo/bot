<?php

/**
 * Основная идея: в reader отправляется пакет данных,
 * исполнитель в процессе чтения программы изменяет данные.
 * После окончания чтения пакет данных сохраняется, а результат
 * работы исполнаяется.
*/

namespace executor;
/**
 * Class Executor
 */
class executor
{
    const act_write = 1;
    const act_execute_function = 2;

    /**
     * perform action list that was planned by reader
     * @param array $actions
     * @param \db\state $state
     * @return \vk\output $output
     */
    public static function perform($payloads, $actions)
    {
        $output = new \vk\output();
        for ($i = 0; $i < count($actions); $i++)
        {
            switch ($actions[$i]['code']) {
                case self::act_write:
                    $output->message($actions[$i]['arg']);
                    break;
                case self::act_execute_function:
                    $namespace = 'functions';
                    $class_name = '';
                    $method_name = $actions[$i]['arg'];
                    switch ($payloads->type_id)
                    {
                        case MOBIDEL_BOT_TYPE:
                            $class_name = 'mobidelfunctions';
                            break;
                    }
                    call_user_func([$namespace.'\\'.$class_name, $method_name], $output, $payloads);
                    break;
            }
        }
        return $output;
    }

    /**
     * convert parameter to required type
     * @param $payloads
     * @param $parameter
     * @return mixed
     */
    private static function parse_parameter($payloads, $parameter)
    {
        global $_prototypes;
        $await_parameter = $payloads->state['read'];
        //get type await parameter
        switch ((int)$payloads->type_id)
        {
            case MOBIDEL_BOT_TYPE:
                $type = $_prototypes['mobidel'][$await_parameter]['type'];
                break;
            default:
                $type = 'string';
                break;
        }
        //convert parameter to required type
        switch ($type)
        {
            case 'integer':
                $parameter = (int)$parameter;
                break;
        }
        return $parameter;
    }

    /**
     * @param string|array $message message from vk user or list params
     * @param object $payloads assistive data
     * @return \vk\output|bool
     * @throws \exception
     */
    public static function execute($event, $payloads)
    {
        $labels = json_decode($payloads->labels, true);
        $commands = json_decode($payloads->commands, true);
        $from_id = $event->object->from_id;
        //check isset state dialog
        if ($payloads->state == null) {
            $payloads->state = \db\state::get_state($payloads->bot_id, $from_id);
        }
        //todo: need resolve it
        //$payloads->state['number_categories'] = $payloads->meta['number_categories'];
        $payloads->state['number_categories'] = 16;

        if ($payloads->state['read'] != '') {// await parameter
            if (
                isset($event->object->payload) &&
                ($payload = json_decode($event->object->payload))
            ) {
                if (isset($payload->value)) {
                    if ($payload->value === 'no action') {
                        return false;
                    }
                    $parameter = self::parse_parameter($payloads, $payload->value);
                } else {
                    throw new \exception('value key undefined');
                }
            } else {
                //todo: непонятный кусок кода, возможно надо убрать
                if (($event->object->text == '...') || ($event->object->text === ' ')) {
                    return false;
                }
                $parameter = self::parse_parameter($payloads, $event->object->text);
            }
        } else {
            $parameter = '';
        }

        try {
            do {
                $actions = reader::_read($parameter, $commands, $labels, $payloads->state);
                $output = self::perform($payloads, $actions);
                \db\state::save($payloads->bot_id, $from_id, $payloads->state);
            } while ($payloads->state['continue'] == 1);
        } catch (\Exception $e) {
            throw new \Exception('Execution error', 500, $e);
        }
        return $output;
    }
}


