<?php

namespace functions;

use vk\keyboard as kb;
use api_crm\MobidelAPI;

/**
 * Class MobidelFunctions implements executing functions from mobidel bots.
 * This class using web Mobidel api or working with cache
 * mobidel functions
 * @package functions
 */
class MobidelFunctions implements FunctionsInterface{
    /**
     * @var MobidelAPI $mobidelApi is instance of
     */
    private static function create_product_card($index, $product)
    {
        $server_url = 'http://images.18r.ru:96/articles/';
        $thumb_url = $server_url.$product->description->id;
        $filename = ROOT.'/temp/card.png';
        $card = new \crm\common\ProductCard();
        $params = [
            'thumb_url' => $thumb_url,
            'header' => $product->description->mobileName,
            'description' => $product->description->mobileDescription,
            'price' => intval($product->description->floatprice).' P',
            'index' => $index,
            'weight' => '1000'.'гр.'
        ];
        if ($product->description->oldfloatprice != $product->description->floatprice ) {
            $params['old_price'] = intval($product->description->oldfloatprice).' P';
        }

        if ($index == 2){
            $params['old_price'] = '500 P';
        }
        $card->draw_card($params)->save($filename);

        //save image in vk TODO: хэшировать имя!!!
        $result = json_decode(\vk\Photos::save_image($filename));
        unlink($filename);
        $photo_id = $result->response[0]->id;
        return $photo_id;
    }
    /**
     * if product with product_id have toppings return true else false
     * @param $product_id
     * @param $payloads
     * @return bool
     */
    private static function have_toppings($product_id, $payloads)
    {
        $row = \db\CacheMenu::get_cache_menu($payloads->group_id);
        $menu = json_decode($row->menu);
        $product = self::get_product_by_id($product_id, $menu);
        if (($product) &&
            (isset($product->description->semiproducts)) &&
            (count($product->description->semiproducts))) {
            $have_toppings = true;
        } else {
            $have_toppings = false;
        }
        return $have_toppings;
    }
    /**
     * get product by id
     * @param $product_id
     * @param $menu
     * @return bool|\stdClass
     */
    private static function get_product_by_id($product_id, $menu)
    {
        for ($i = 0; $i < count($menu); $i++) {
            for ($j = 0; $j < count($menu[$i]->childrens); $j++) {
                $product = $menu[$i]->childrens[$j];
                if ($product->description->id == $product_id) {
                    return $product;
                } elseif (count($product->childrens) > 0) {
                    for ($k = 0; $k < count($product->childrens); $k++) {
                        if ($product->childrens[$k]->description->id == $product_id) {
                            return $product->childrens[$k];
                        }
                    }
                }
            }
        }
        return false;
    }
    /**
     * get map price parameters for configuring product
     * @param $product
     * @return array
     */
    private static function build_map($product)
    {
        $map = [
            'id' => [],
            'name' => [],
            'configuration' => []
        ];
        foreach ($product->childrens as $i => $child) {
            foreach ($child->description->priceParameters as $j => $priceParameter) {
                $param_id = $priceParameter->priceParameterID;
                $param_name = $priceParameter->priceParameterName;
                $value_id = $priceParameter->priceParameterValueID;
                $value_name = $priceParameter->priceParameterValueName;
                //add ids in map
                if (isset($map['id'][$param_id])) {
                    if (array_search($value_id, $map['id'][$param_id]) === false) {
                        array_push($map['id'][$param_id], $value_id);
                    }
                } else {
                    $map['id'][$param_id] = [ $value_id ];
                    $map['configuration'][$param_id] = -1;
                }
                //add names in map
                if (isset($map['name'][$param_name])) {
                    if (array_search($value_name, $map['name'][$param_name]) === false) {
                        array_push($map['name'][$param_name], $value_name);
                    }
                } else {
                    $map['name'][$param_name] = [ $value_name ];
                }
            }
        }

        return $map;
    }
    /**
     * find product with price parameters by parent product and configuration
     * @param $parent_product
     * @param $configuration
     */
    private static function find_product_by_configuration($product, $configuration)
    {
        $result = false;
        $i = 0;
        while (($i < count($product->childrens)) && ($result === false)) {
            $child = $product->childrens[$i];
            $is_matches = true;
            $j = 0;
            while (($j < count($child->description->priceParameters)) && ($is_matches === true)) {
                $id_parameter = $child->description->priceParameters[$j]->priceParameterID;
                $id_value = $child->description->priceParameters[$j]->priceParameterValueID;
                if (!isset($configuration[$id_parameter])) {
                    throw new \Exception("unexpected parameter", 1);
                } elseif ($configuration[$id_parameter] != $id_value) {
                    $is_matches = false;
                }
                $j++;
            }
            if ($is_matches === true) {
                $result = $child;
            }
            $i++;
        }
        return $result;
    }
    /**
     * get semiproduct by id
     * @param $semiproduct_id
     * @param $product
     * @return bool|array|object
     */
    private static function get_topping_by_id($semiproduct_id, $product)
    {
        $semiproducts = $product->description->semiproducts;
        $i = 0;
        $max = count($semiproducts);
        while (($i < $max) && ($semiproduct_id != $semiproducts[$i]->id)) {
            $i++;
        }
        if ($i == $max) {
            $semiproduct = false;
        } else {
            $semiproduct = $semiproducts[$i];
        }
        return $semiproduct;
    }

    /**
     * @param $warehouseID
     * @return false|\stdClass
     */
    private static function get_warehouse_by_id($warehouseID)
    {
        $warehouses = \api_crm\MobidelApi::getWarehouses();
        $i = 0;
        $max = count($warehouses);
        $warehouse = false;
        while (($i < $max) && (!$warehouse)) {
            if ($warehouses[$i]->id == $warehouseID) {
                $warehouse = $warehouses[$i];
            }
        }
        return $warehouse;

    }

    /**
     * shell for MobidelAPI::check_order_sum
     * TODO: избавиться от этой функции
     * наличие этой функции - проблема, её не должно было быть, но структура
     * хранения продуктов в корзине отлична от той, котороя требуется при работу
     * с api mobidel.
     * @param $articles
     * @return mixed
     */
    public static function check_order_sum($payloads, $articles)
    {
        $items = [];
        foreach ($articles as $key => $item){
            $semiproducts = [];
            foreach ($item['semiproducts'] as $semiproductId => $semiproduct){
                array_push($semiproducts, [
                    'idArticle' => $semiproductId,
                    'quantity' => $semiproduct['quantity']
                ]);
            }
            array_push($items, [
                'idArticle' => $key,
                'quantity' => $item['quantity'],
                'semiproducts' => $semiproducts
            ]);
        }
        $bonusPay = $payloads->state['bonusPay'];
        $order_sum = \api_crm\mobidel\Shop::checkOrderSumm($items, $bonusPay);
        return $order_sum;
    }
    /**
     * @param \vk\Output $output
     */
    private static function print_check($output, $payloads, $articles)
    {
        $output->message(str_repeat('-', 54));
        $output->message('           Ч е к');
        $order_sum = self::check_order_sum($payloads, $articles);
        $prices = [
            'delivery' => [
                'label' => 'Доставка',
                'placeholder_length' => 34,
                'count' => $order_sum[1]->deliveryPrice
            ],
            'discount' => [
                'label' => 'Скидка',
                'placeholder_length' => 38,
                'count' => $order_sum[1]->discountSum
            ],
            'count' => [
                'label' => 'Общая сумма заказа',
                'placeholder_length' => 14,
                'count' => $order_sum[1]->orderSum
            ],
            'checkout' => [
                'label' => 'Итого',
                'placeholder_length' => 41,
                'count' => $order_sum[1]->orderSum+$order_sum[1]->deliveryPrice
            ],
        ];
        foreach ($prices as $key => $price) {
            $str = $price['label'];
            $placeholder_length = $price['placeholder_length'] - strlen($price['count'])*2;
            if ($placeholder_length < 4) {
                $placeholder_length = 4;
            }
            $str .= str_repeat('.', $placeholder_length).$price['count'].'p.';
            $output->message($str);
        }
        $output->message(str_repeat('-', 54));
    }
    /**
     * @param \vk\Output $output
     */
    private static function print_products_in_cart($output, $payloads, $articles)
    {
        $row = \db\CacheMenu::get_cache_menu($payloads->group_id);
        $menu = json_decode($row->menu);
        $index = 1;
        foreach ($articles as $key => $item){
            $product = self::get_product_by_id($key, $menu);
            $output->message($index.") ".$product->description->mobileName.' '."x{$item['quantity']}");
            if (count($item['semiproducts']) > 0) {//have semiproducts
                $output->message(str_repeat('&#8199;', 2).'Добавки:');
                $semiproducts = $product->description->semiproducts;
                for ($i = 0; $i < count($semiproducts); $i++) {
                    $semiproducts_id = $semiproducts[$i]->id;
                    if (isset($item['semiproducts'][$semiproducts_id])) {
                        if ($item['semiproducts'][$semiproducts_id]['quantity'] > 0) {
                            $quantity = $item['semiproducts'][$semiproducts_id]['quantity'];
                            //TODO: need carry word by space else if it impossible, by fixed length

                            $output->message(
                                str_repeat('&#8199;', 3).'—&#8199;'.
                                $semiproducts[$i]->name.' x'.$quantity
                            );
                        }
                    }
                }
            }
            $index++;
        }
    }

    /**
     * @param \vk\Output $output
     */
    public static function create_order($output, $payloads)
    {
    }
    /**
     * print order info
     * @param \vk\Output $output
     */
    public static function order_preview($output, $payloads)
    {
        //TODO: тут проверку на корректность заполнения всех полей
        $correct = true;
        if ($correct) {
            $output->message("-----<Информация о заказе>----");
            $output->message('Имя: '.$payloads->state['name']);
            $output->message('*Телефон: '.$payloads->state['phone']);
            $output->message('*Улица: '.$payloads->state['street']);
            $output->message('*Подъезд: '.$payloads->state['entrance']);
            $output->message('*Квартира: '.$payloads->state['flat']);
            $output->message('Комментарий: '.$payloads->state['comment']);
            if ($payloads->state['independently'] == 1) {
                $output->message('Получение: Самовывоз');
            } elseif ($payloads->state['independently'] == 0) {
                $message = 'Получение: Доставка';
                $warehouse_id = $payloads->state['warehouseID'];
                $warehouse = self::get_warehouse_by_id($warehouse_id);
                if ($warehouse) {
                    $message .= '('.$warehouse->name.')';
                }
                $output->message($message);
            }
            $output->message('Использовать бонусы: '.$payloads->state['bonusPay']);
        } else {
            $output->message('Не все данные были указаны.');
        }
        $kb = kb\patterns\MobidelPattern::preview_order(0);
        $output->keyboard($kb);
    }

    //TODO: remove both functions make_order_prev and make_order_next
    public static function make_order_prev($output, $payloads)
    {
    }
    public static function make_order_next($output, $payloads)
    {
    }
    /**
     * write categories and print keyboard
     * @param \vk\Output $output
    */
    public static function print_categories($output, $payloads)
    {
        $categories = [];
        $row = \db\CacheMenu::get_cache_menu($payloads->group_id);
        $menu = json_decode($row->menu);//меню на Localhost не читается

        $output->message("Выберите категорию меню");
        for ($i = 0; $i < count($menu); $i++) {
            $category_name = $menu[$i]->description->mobileName;
            array_push($categories, $category_name);
        }
        $articles = \db\Cart::fetch_articles($payloads->bot_id, $payloads->vk_user_id);
        $output_order_button = false;
        if (($articles !== false) && (count($articles))) {
            $output_order_button = true;
        }
        $kb = kb\patterns\MobidelPattern::categories_kb($categories, $output_order_button, false);
        $output->keyboard(json_encode($kb, JSON_UNESCAPED_UNICODE));
    }
    /**
     * @param \vk\Output $output
     */
    public static function print_products($output, $payloads)
    {
        $row = \db\CacheMenu::get_cache_menu($payloads->group_id);
        $menu = json_decode($row->menu);
        $index_of_category = $payloads->state['index_of_category'];
        $index_of_page_menu = $payloads->state['index_of_page_menu'];
        $category = $menu[$index_of_category];
        $per_page = 5; //max products on one page
        $max_index_of_page_menu = ceil(count($category->childrens)/$per_page);
        $offset = $per_page * ($index_of_page_menu - 1);
        $products = array_slice($category->childrens, $offset, $per_page);
        $number_products = count($category->childrens);
        $cards = \db\CacheCard::fetch_cards($payloads->group_id);
        $cards_changed = false;

        $payloads->state['max_page_menu'] = $max_index_of_page_menu;
        //print menu
        $introduce =
            str_repeat('-', 20).
            '< '.
            $category->description->mobileName.
            " $index_of_page_menu/".
            $max_index_of_page_menu.
            ' >'.
            str_repeat('-', 20);

        //print cards
        $output->message($introduce);
        foreach ($products as $i => $product)
        {
            $product_id = $product->description->id;
            $index = array_search($product_id, $cards->product_id);
            if ($index === false) {
                $photo_id = self::create_product_card($i+$offset+1, $product);
                array_push($cards->product_id, $product_id);
                array_push($cards->photo_id, (string)$photo_id);
                $cards_changed = true;
                $output->attachment("photo-".$payloads->group_id."_".$photo_id);
            } else{
                $output->attachment("photo-".$payloads->group_id."_".$cards->photo_id[$index]);
            }
        }
        if ($cards_changed) {
            \db\CacheCard::update_card($payloads->bot_id, $cards);
        }
        //print keyboard
        $kb = kb\patterns\MobidelPattern::products_kb(
            $index_of_page_menu,
            $per_page,
            $number_products,
            $payloads->state['long_step'],
            false
        );
        $output->keyboard(json_encode($kb, JSON_UNESCAPED_UNICODE));
    }
    /**
     * determine type of product
     * @param \vk\Output $output
     * @param $payloads
     */
    public static function select_product($output, $payloads)
    {
        define('SIMPLE_PRODUCT', 0);
        define('CONFIGURE_PRODUCT', 1);
        define('PRODUCT_NOT_FOUND', -1);
        $row = \db\CacheMenu::get_cache_menu($payloads->group_id);
        $menu = json_decode($row->menu);
        $index_of_category = $payloads->state['index_of_category'];
        $category = $menu[$index_of_category];
        $product_index = $payloads->state['menu_parameter'];
        //determine type of product
        if (isset($category->childrens[$product_index])) {
            $product = $category->childrens[$product_index];
            $payloads->state['id_selected_product'] = $product->description->id;
            if (count($product->childrens) && ($product->childrens[0]->description->usePriceParameter == "1")){
                $payloads->state['type_of_selected_product'] = CONFIGURE_PRODUCT;
                $payloads->state['price_parameters_list'] = self::build_map($product);
            } else {
                $payloads->state['type_of_selected_product'] = SIMPLE_PRODUCT;
            }
        } else {
            $payloads->state['type_of_selected_product'] = PRODUCT_NOT_FOUND;
        }
    }
    /**
     * print product
     * @param \vk\Output $output
     * @param $payloads
     */
    public static function print_product($output, $payloads)
    {
        define('SIMPLE_PRODUCT', 0);
        define('CONFIGURE_PRODUCT', 1);
        define('PRODUCT_NOT_FOUND', -1);
        $row = \db\CacheMenu::get_cache_menu($payloads->group_id);
        $menu = json_decode($row->menu);
        $product = self::get_product_by_id($payloads->state['id_selected_product'], $menu);
        switch ($payloads->state['type_of_selected_product']) {
            case SIMPLE_PRODUCT:
                $name = $product->description->mobileName;
                $have_toppings = self::have_toppings(
                    $payloads->state['id_selected_product'],
                    $payloads
                );
                $article = \db\Cart::get_article($product->description->id, $payloads->bot_id, $payloads->vk_user_id);
                if ($article === false) {
                    $quantity = 0;
                } else {
                    $quantity = $article['quantity'];
                }
                $kb = kb\patterns\MobidelPattern::simple_product_control_kb($quantity, $have_toppings);
                $output->message("Вы выбрали продукт '$name'");
                $output->keyboard($kb);
                break;
            case CONFIGURE_PRODUCT:
                $article = \db\Cart::get_article($product->description->id, $payloads->bot_id, $payloads->vk_user_id);
                if ($article === false) {
                    $quantity = 0;
                    $payloads->state['map_price_parameters'] = self::build_map($product);
                    $kb = kb\patterns\MobidelPattern::configure_product_control_kb($quantity);
                    $output->message("Выбранный продукт является конфигурируемым, " .
                        "для детальной настрройки \n нажмите \"Конфигурация\"");
                } else {
                    $quantity = $article['quantity'];
                    $have_toppings = self::have_toppings($payloads->state['id_selected_product'], $payloads);
                    $kb = kb\patterns\MobidelPattern::simple_product_control_kb($quantity, $have_toppings);
                }
                $output->keyboard($kb);
                break;
            case PRODUCT_NOT_FOUND:
                break;
        }
    }
    /**
     * @param \vk\Output $output
     */
    public static function inc_product($output, $payloads)
    {
        $quantity = \db\Cart::inc_product(
            $payloads->bot_id,
            $payloads->vk_user_id,
            $payloads->state['id_selected_product']
        );
        $have_toppings = self::have_toppings(
            $payloads->state['id_selected_product'],
            $payloads
        );
        $output->message('add');
        $kb = kb\patterns\MobidelPattern::simple_product_control_kb($quantity, $have_toppings);
        $output->keyboard(json_encode($kb, JSON_UNESCAPED_UNICODE));
    }
    /**
     * @param \vk\Output $output
     */
    public static function dec_product($output, $payloads)
    {
        $quantity = \db\Cart::dec_product(
            $payloads->bot_id,
            $payloads->vk_user_id,
            $payloads->state['id_selected_product']
        );
        $have_toppings = self::have_toppings(
            $payloads->state['id_selected_product'],
            $payloads
        );
        $output->message('remove');
        $kb = kb\patterns\MobidelPattern::simple_product_control_kb($quantity, $have_toppings);
        $output->keyboard(json_encode($kb, JSON_UNESCAPED_UNICODE));
    }
    /**
     * @param \vk\Output $output
     */
    public static function remove_product($output, $payloads)
    {
        \db\Cart::remove_product(
            $payloads->bot_id,
            $payloads->vk_user_id,
            $payloads->state['id_selected_product']
        );
        $output->message('remove all');
        $kb = kb\patterns\MobidelPattern::simple_product_control_kb(0, false);
        $output->keyboard(json_encode($kb, JSON_UNESCAPED_UNICODE));
    }

    /**
     * print topping menu for selected product
     * @param \vk\Output $output
     */
    public static function print_toppings($output, $payloads)
    {
        $row = \db\CacheMenu::get_cache_menu($payloads->group_id);
        $menu = json_decode($row->menu);
        $product_id = $payloads->state['id_selected_product'];
        if ($product = self::get_product_by_id($product_id, $menu)) {
            $semiproducts = $product->description->semiproducts;
            $output->message('Доступны следующие добавки:');
            for ($i = 0; $i < count($semiproducts); $i++) {
                $output->message(($i+1).') '.$semiproducts[$i]->name);
            }
            $kb = kb\patterns\MobidelPattern::semiproducts_kb($semiproducts);
            $output->keyboard(json_encode($kb, JSON_UNESCAPED_UNICODE));
        } else {
            $output->message("Ошибка, продукт с идентификатором '$product_id' не найден!");
            $output->message("Пожалуйста, сообщите об этом администратору.");
            $output->message("Приносим свои извинения за неудобства");
        }
    }
    /**
     * @param \vk\Output $output
     */
    public static function select_topping($output, $payloads)
    {
        $row = \db\CacheMenu::get_cache_menu($payloads->group_id);
        $menu = json_decode($row->menu);
        $product = self::get_product_by_id($payloads->state['id_selected_product'], $menu);
        if ($product) {
            $semiproduct_index = $payloads->state['menu_parameter'];
            $semiproduct_id = $product->description->semiproducts[$semiproduct_index]->id;
            $payloads->state['id_selected_semiproduct'] = $semiproduct_id;

        }
    }

    /**
     * @param \vk\Output $output
     */
    public static function print_topping($output, $payloads)
    {
        $row = \db\CacheMenu::get_cache_menu($payloads->group_id);
        $menu = json_decode($row->menu);
        $product = self::get_product_by_id($payloads->state['id_selected_product'], $menu);
        $semiproduct = self::get_topping_by_id($payloads->state['id_selected_semiproduct'], $product);
        //print name
        $semiproduct_name = $semiproduct->name;
        $output->message("Вы выбрали добавку '$semiproduct_name'");
        //get quantity in cart
        $article = \db\Cart::get_article($product->description->id, $payloads->bot_id, $payloads->vk_user_id);
        if (($article !== false) && (isset($article['semiproducts'][$semiproduct->id]))) {
            $quantity = $article['semiproducts'][$semiproduct->id]['quantity'];
        } else {
            $quantity = 0;
        }
        $kb = kb\patterns\MobidelPattern::topping_control_kb($quantity);
        $output->keyboard($kb);
    }
    /**
     * @param \vk\Output $output
     */
    public static function inc_topping($output, $payloads)
    {
        $quantity = \db\Cart::inc_semiproduct(
            $payloads->bot_id,
            $payloads->vk_user_id,
            $payloads->state['id_selected_product'],
            $payloads->state['id_selected_semiproduct']
        );
        $output->message('add topping');
        $kb = kb\patterns\MobidelPattern::topping_control_kb($quantity);
        $output->keyboard($kb);
    }
    /**
     * @param \vk\Output $output
     */
    public static function dec_topping($output, $payloads)
    {
        $quantity = \db\Cart::dec_semiproduct(
            $payloads->bot_id,
            $payloads->vk_user_id,
            $payloads->state['id_selected_product'],
            $payloads->state['id_selected_semiproduct']
        );
        $output->message('remove topping');
        $kb = kb\patterns\MobidelPattern::topping_control_kb($quantity);
        $output->keyboard($kb);
    }
    /**
     * @param \vk\Output $output
     */
    public static function remove_topping($output, $payloads)
    {
        \db\Cart::remove_semiproduct(
            $payloads->bot_id,
            $payloads->vk_user_id,
            $payloads->state['id_selected_product'],
            $payloads->state['id_selected_semiproduct']
        );
        $output->message('drop topping');
        $kb = kb\patterns\MobidelPattern::topping_control_kb(0);
        $output->keyboard($kb);
    }

    /**
     * @param \vk\Output $output
     */
    public static function print_cart($output, $payloads)
    {
        $articles = \db\Cart::fetch_articles($payloads->bot_id, $payloads->vk_user_id);
        if ($articles === false) {
            $output->message("Корзина пуста");
            $kb = kb\patterns\MobidelPattern::cart_kb(0);
        } else {
            $output->message(str_repeat('-', 54));
            $output->message('       К о р з и н а');
            $output->message(str_repeat('-', 54));
            $kb = kb\patterns\MobidelPattern::cart_kb(count($articles));
            self::print_products_in_cart($output, $payloads, $articles);
        }

        $output->keyboard($kb);
    }
    /**
     * @param \vk\Output $output
     */
    public static function print_price_parameter($output, $payloads)
    {
        $map = $payloads->state['map_price_parameters'];
        $i = 0;
        $max = count($map['configuration']);
        $keys = array_keys($map['configuration']);
        while (($i < $max) && ($map['configuration'][$keys[$i]] != -1)) {
            $i++;
        }

        if ($i < $max) {
            $id_current_parameter = $keys[$i];
            $kb = kb\patterns\MobidelPattern::price_parameter_kb($id_current_parameter, $map);
            //print parameter name
            $id_keys = array_keys($map['id']);
            $index_parameter = array_search($id_current_parameter, $id_keys);
            $name_keys =  array_keys($map['name']);
            $parameter_name = $name_keys[$index_parameter];
            $output->message('Выберите '.$parameter_name);
        } else {
            $kb = kb\patterns\MobidelPattern::end_of_configuration_kb();
        }
        $output->keyboard($kb);
    }
    /**
     * @param \vk\Output $output
     */
    public static function print_previous_price_parameter($output, $payloads)
    {
        $output->message('no implements');
    }

    /**
     * TODO: rename to select_value
     * save selected parameter
     * @param \vk\Output $output
     */
    public static function select_value($output, $payloads)
    {
        $map = $payloads->state['map_price_parameters'];
        $i = 0;
        $max = count($map['configuration']);
        $keys = array_keys($map['id']);
        while (($i < $max) && ($map['configuration'][$keys[$i]] != -1)) {
            $i++;
        }
        if ($i < $max) {
            $value_id = $map['id'][$keys[$i]][$payloads->state['menu_parameter']];
            $payloads->state['map_price_parameters']['configuration'][$keys[$i]] = $value_id;
            //print message
            $keys = array_keys($map['name']);
            $parameter_name = $keys[$i];
            $value_name = $map['name'][$keys[$i]][$payloads->state['menu_parameter']];
            $output->message("$parameter_name: $value_name");
        }
    }
    /**
     * save configured product in cart
     * @param \vk\Output $output
     */
    public static function end_configuration($output, $payloads)
    {
        $row = \db\CacheMenu::get_cache_menu($payloads->group_id);
        $menu = json_decode($row->menu);
        $parent_product = self::get_product_by_id($payloads->state['id_selected_product'], $menu);
        // check isset this product in cart
        $configuration = $payloads->state['map_price_parameters']['configuration'];
        try {
            $product = self::find_product_by_configuration($parent_product, $configuration);
            if ($product === false) {
                $payloads->state['id_selected_product'] = '';
            } else {
                $payloads->state['id_selected_product'] = $product->description->id;
            }
        } catch (\Exception $e){
            $payloads->state['id_selected_product'] = '';
            $output->message('Меню было обновлено, установленная конфигурация больше не дейтсвительна.');
        }
    }
    /**
     * @param \vk\Output $output
     */
    public static function cart_edit_product($output, $payloads)
    {
        $articles = \db\Cart::fetch_articles($payloads->bot_id, $payloads->vk_user_id);
        $index = $payloads->state['menu_parameter'];
        $keys = array_keys($articles);
        $payloads->state['id_selected_product'] = $keys[$index];
        $output->message('Редактирование продукта с id '.$keys[$index]);
    }


    private static function auth_client($payloads)
    {
        define('AUTHORISED', '0');
        define('UNREGISTERED', '3');
        $login = $payloads->state['login'];
        $pass = $payloads->state['password'];
        $json = \api_crm\mobidel\Client::authClient($login, $pass);
        $res = json_decode($json, true);
        $result = false;
        switch ($res['result']) {
            case AUTHORISED:
                $result = true;
                break;
            case UNREGISTERED:
                $result = false;
                break;
        }
        return $result;
    }
    /**
     * print order
     * @param \vk\Output $output
     */
    public static function print_order($output, $payloads)
    {
        if (self::auth_client($payloads)) {
            $articles = \db\Cart::fetch_articles($payloads->bot_id, $payloads->vk_user_id);
            if ($articles === false) {
                $output->message('Корзина пуста.');
                $output->message('Заказывать нечего.');
            } else {
                $output->message(str_repeat('-', 54));
                $output->message('       К о р з и н а');
                $output->message(str_repeat('-', 54));
                self::print_products_in_cart($output, $payloads, $articles);
                self::print_check($output, $payloads, $articles);
            }
            $pm = '';
            $pm_index = $payloads->state['paymentMethod'];
            $pm_map = [
                'Наличные',
                'Безналичные',
                'Онлайн'
            ];
            if ($pm_index > -1) {
                $pm = $pm_map[$pm_index];
            }
            $independently = $payloads->state['independently'];
            $bonusPay = $payloads->state['bonusPay'];
            $kb = kb\patterns\MobidelPattern::order_kb($pm, $independently, $bonusPay);
        } else {
            $output->message('Для оформления заказа требуется зарегистрироваться в mobidel crm.');
            $output->message('Надо указать номер.');
            $kb = kb\patterns\MobidelPattern::register_kb();
        }
        $output->keyboard($kb);
    }
    /**
     * @param \vk\Output $output
     */
    public static function order_register($output, $payloads)
    {
        $output->message('Укажите номер телефона, в формате +7 999 999 99 99, на который придёт смс.');
    }

    /**
     * shell for \api_crm\mobidel\Client::createClient
     * @param $phone
     * @param $password
     * @return bool
     */
    private static function register($phone, $password)
    {
        $json = \api_crm\mobidel\Client::createClient($phone, $password);
        $res = json_decode($json);
        return $res;
    }

    /**
     * print order
     * @param \vk\Output $output
     */
    public static function send_sms($output, $payloads)
    {
        $phone = $payloads->state['login'];
        $password = '123456';
        if (preg_match("/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/", $phone) === 1) {
            $payloads->state['input_correct'] = 1;
            $json = \api_crm\mobidel\Client::createClient($phone, $password);
            $res = json_decode($json);
            if ($res->result === '0') {
                $payloads->state['deviceID'] = $res->deviceID;
                $kb = kb\patterns\MobidelPattern::sms();
                $output->message('Смс с паролем отправлено на номер '.$phone);
                $output->keyboard($kb);
            } else {
                $output->message('Ошибка регистрации');
            }
        } else {
            $payloads->state['input_correct'] = 0;
            $output->message('Номер не корректен, повторите ввод номера');
        }
    }
    /**
     * @param \vk\Output $output
     */
    public static function check_pin($output, $payloads)
    {
        $pin = $payloads->state['input'];
        $phone = $payloads->state['login'];
        $deviceID = $payloads->state['deviceID'];
        $json = \api_crm\mobidel\Client::checkPIN($pin, $phone, $deviceID);
        $res = json_decode($json);
        if ($res->result === '0') {

        }
    }

    /**
     * print order
     * @param \vk\Output $output
     */
    public static function print_order_pm($output, $payloads)
    {
        $pm_index = $payloads->state['paymentMethod'];
        $pm_map = [
            'Наличные',
            'Безналичные',
            'Онлайн'
        ];
        if ($pm_index === -1) {
            $output->message('Выберите способ оплаты');
        } else {
            $output->message('Выберите способ оплаты');
            $output->message('Текущий: '.$pm_map[$pm_index]);
        }
        $kb = kb\patterns\MobidelPattern::order_pm_kb($pm_index, $pm_map);
        $output->keyboard($kb);
    }
    /**
     * print order initializing
     * @param \vk\Output $output
     */
    public static function print_order_init($output, $payloads)
    {
        $fields = [
            [
                'label' => 'Телефон',
                'required' => true,
                'value' => $payloads->state['phone']
            ],[
                'label' => 'Улица',
                'required' => true,
                'value' => $payloads->state['street']
            ],[
                'label' => 'Дом',
                'required' => true,
                'value' => $payloads->state['home']
            ],[
                'label' => 'Подъезд',
                'required' => false,
                'value' => $payloads->state['entrance']
            ],[
                'label' => 'Комната',
                'required' => false,
                'value' => $payloads->state['room']
            ],[
                'label' => 'Комментарий',
                'required' => false,
                'value' => $payloads->state['comment']
            ]
        ];
        $index = $payloads->state['order_init_index'];
        $chunks = array_chunk($fields, 3);
        $max_index = count($chunks) -1;
        $payloads->state['order_init_max_index'] = $max_index;
        if ($index >= $max_index) {
            $fields = $chunks[$max_index];
        } else {
            $fields = $chunks[$index];
        }
        $output->message('Оформить заказ');
        $kb = kb\patterns\MobidelPattern::order_init_kb($fields, $index+1, $max_index+1);
        $output->keyboard($kb);
    }
    /**
     * read order init parameter value
     * @param \vk\Output $output
     */
    public static function order_init_read_parameter($output, $payloads)
    {
        $fields = [
            [
                'label' => 'Телефон',
                'name'  => 'phone',
                'value' => $payloads->state['phone']
            ],[
                'label' => 'Улица',
                'name'  => 'street',
                'value' => $payloads->state['street']
            ],[
                'label' => 'Дом',
                'name'  => 'home',
                'value' => $payloads->state['home']
            ],[
                'label' => 'Подъезд',
                'name'  => 'entrance',
                'value' => $payloads->state['entrance']
            ],[
                'label' => 'Комната',
                'name'  => 'room',
                'value' => $payloads->state['room']
            ],[
                'label' => 'Комментарий',
                'name'  => 'comment',
                'value' => $payloads->state['comment']
            ]
        ];
        $index = $payloads->state['order_init_index'];
        $chunks = array_chunk($fields, 3);
        $max_index = count($chunks) -1;
        $payloads->state['order_init_max_index'] = $max_index;
        if ($index >= $max_index) {
            $fields = $chunks[$max_index];
        } else {
            $fields = $chunks[$index];
        }
        $field_index = $payloads->state['menu_parameter'];
        if (!isset($fields[$field_index])) {
            $field_index = 0;
        }
        $payloads->state['order_init_name_read_parameter'] = $fields[$field_index]['name'];
        if ($fields[$field_index]['value']) {
            $output->message('Текущий '.$fields[$field_index]['label'].': '.$fields[$field_index]['value']);
            $output->message('Для изменения имени введите новое');
        } else {
            $output->message('Текущий '.$fields[$field_index]['label'].': не указан');
            $output->message('Для указания имени введите новое');
        }
    }
    /**
     * @param \vk\Output $output
     */
    public static function order_init_save_parameter($output, $payloads)
    {
        $parameter_name = $payloads->state['order_init_name_read_parameter'];
        $payloads->state[$parameter_name] = $payloads->state['input'];
    }
    /**
     * @param \vk\Output $output
     */
    public static function print_order_ow($output, $payloads)
    {
        $independently = $payloads->state['independently'];
        $output->message('Способ получения');
        $kb = kb\patterns\MobidelPattern::order_ow_kb($independently);
        $output->keyboard($kb);
    }
    /**
     * @param \vk\Output $output
     */
    public static function select_warehouse($output, $payloads)
    {
        $warehouse_index = $payloads->state['menu_parameter'];
        $warehouses = \api_crm\MobidelApi::getWarehouses();
        $payloads->state['warehouseID'] = $warehouses[$warehouse_index]->id;
    }
    /**
     * @param \vk\Output $output
     */
    public static function print_warehouse($output, $payloads)
    {
        $warehouses = \api_crm\MobidelApi::getWarehouses();
        for($i = 0; $i < count($warehouses); $i++){
            $output->message(($i+1).') '.$warehouses[$i]->name);
        }
        $kb = kb\patterns\MobidelPattern::warehouses($warehouses);
        $output->keyboard($kb);
    }
    public static function toggle_bonusPay($output, $payloads)
    {
        if ($payloads->state['bonusPay'] == 1) {
            $payloads->state['bonusPay'] = 0;
        } else {
            $payloads->state['bonusPay'] = 1;
        }
    }

    /**
     * @param \vk\Output $output
     */
    public static function outMenu($output)
    {
        $kb = kb\patterns\MobidelPattern::emptyKeyboard();
        $output->keyboard($kb);
    }
}
