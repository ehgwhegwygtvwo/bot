<?php

namespace translator;

class TranslatorMobidel extends Translator
{
    private static
        $switch_count = 0,
        $menu_count = 0;

    /**
     * @param $command
     */
    private static function block_switch($command)
    {
        self::push([
            'type' => 'write',
            'args' => [
                $command['message']
            ]
        ]);
        for ($i=0; $i < count($command['cases']); $i++) {
            self::push([
                'type' => 'write',
                'args' => [
                    ($i+1).'. '.$command['cases'][$i]['title']
                ]
            ]);
        }
        self::push([
            'type' => 'read',
            'args' => [
                'choice'
            ]
        ]);
        //create map
        for ($i=0; $i < count($command['cases']); $i++) {
            $case_label = 'sw-'.self::$switch_count.'-case-'.$i;
            //compare by index
            self::push([
                'type' => 'compare',
                'args' => [
                    $i+1,
                ]
            ]);
            self::push([
                'type' => 'je',
                'args' => [
                    $case_label
                ]
            ]);
            //compare by value
            self::push([
                'type' => 'compare',
                'args' => [
                    $command['cases'][$i]['title']
                ]
            ]);
            self::push([
                'type' => 'je',
                'args' => [
                    $case_label
                ]
            ]);
        }
        //create body cases
        for ($i=0; $i < count($command['cases']); $i++) {
            $case_label = 'sw-'.self::$switch_count.'-case-'.$i;
            self::label($case_label);
            self::parse($command['cases'][$i]['commands']);
            self::push([
                'type' => 'jump',
                'args' => [
                    'sw-end-'.self::$switch_count
                ]
            ]);
        }
        // self::label($case_label, -1);
        self::label('sw-end-'.self::$switch_count);
    }

    /**
     * @param $command
     */
    private static function block_menu($command)
    {
        if ($command['label']) {
            self::label($command['label']);
        }
        ComponentTranslator::parse('mobidel/menu.logic', '-menu-'.self::$menu_count);
    }

    /**
     * Take an array of logic commands and transform it
     * in execution programm
     * @param $commands
     */
    private static function parse($commands)
    {
        foreach ($commands as $index => $command) {
            switch ($command['type']) {
                case 'read':
//                    self::push($command);
                    break;
                case 'write':
                    self::push(strtoupper($command['type']), [$command['message']], $command['label']);
                    break;
                case 'jump':
//                    self::push($command);
                    break;
                case 'stop':
                    self::push(strtoupper($command['type']), [$command['comment']], $command['label']);
                    break;
                case 'compare':
//                    self::push($command);
                    break;
                case 'switch':
                    self::block_switch($command);
                    self::$switch_count += 1;
                    break;
                case 'menu':
                    self::block_menu($command);
                    self::$menu_count += 1;
                    break;
            }
        }
    }

    public static function translate($source)
    {
        self::goto_begin();
        //hand procedures
        if (isset($source['procedures'])) {
            foreach ($source['procedures'] as $name => $procedure) {
                self::parse($procedure['commands']);
            }
        }
        //hand main stack commands
        self::parse($source['commands']);
        self::goto_begin();
        $commands = self::$Stack_commands;
        $labels = self::$Stack_labels;
        return [ 'commands' => $commands, 'labels' => $labels ];
    }
}