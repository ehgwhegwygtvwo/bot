<?php

namespace translator;

/**
 * Have a some methods for all interpreters and constants
 * TODO: не происходит проверка на переходы на не существующие метки
 */
class Translator
{
    const
        NUMBER_OF_COMMAND = [
            //взаимодействие
            'WRITE' => 100,
            'READ' => 101,
            //переходы
            'JMP' => 200,
            'JE' => 201,
            'JN' => 202,
            'JNE' => 203,
            'JL' => 204,
            'JLE' => 205,
            'JG' => 206,
            'JGE' => 207,
            'PROC' => 208,
            //служебные
            'EXEC' => 300,
            'MOV' => 301,
            'STOP' => 302,
            'CMP' => 303,
            'ENDP' => 304,
            'SUB' => 305,
            'ADD' => 306,
            'PUSH' => 307,
            'POP' => 308
        ];

    protected static
        $index = 0,
        $Stack_commands = [],
        $Stack_labels = [];

    /**
     * Since procedures handed first begin of programm
     * not may be begin of programm
     */
    protected static function goto_begin()
    {
        self::push('JMP', ['begin']);
    }

    /**
     * Push command in stack commands
     * @param string $name command name
     * @param null|array $args
     * @param null|String|array $label
     */
    protected static function push($name, $args = null, $label = null)
    {
        if ($label) {
            if (is_array($label)) {
                for ($i = 0; $i < count($label); $i++) {
                    self::label($label[$i]);
                }
            } else {
                try{
                    self::label($label);
                } catch (\Exception $e) {
                    $params = json_encode(func_get_args());
                    throw new \Exception("error push command ".$params, 0, $e->getPrevious());
                }
            }
        }
        array_push(self::$Stack_commands, [
            'code' => self::NUMBER_OF_COMMAND[$name],
            'name' => $name, //only debug
            'labels' => $label, //only debug
            'args' => $args
        ]);
        self::$index += 1;
    }

    /**
     * save commit of index to array labels by name $name
     * @param $name
     * @param int $offset offset from current index stack commands
     */
    protected static function label($name = '', $offset = 0)
    {
        if (isset(self::$Stack_labels[$name])) {
            throw new \Exception("label \"$name\" already exists");
        }
        if ($name !== '') {
            self::$Stack_labels[$name] = self::$index + $offset;
        }
    }
}