<?php

namespace translator;

/**
 * Class ComponentTranslator read and translate *.asm files
 */
class ComponentTranslator extends Translator
{
    /**
     * @var null|resource - handler of file with asm logic component
     */
    private static $handle = null;
    private static $commands;
    private static $labels;
    private static $labels_map;

    /**
     * commands map, index is name command, value is arguments types list
     * @var array
     */
    private static $command_prototypes = [
        'WRITE' => ['mixed'],
        'READ' => ['name'],
        //переходы
        'JMP' => ['name'],
        'JE' => ['name'],
        'JN' => ['name'],
        'JNE' => ['name'],
        'JL' => ['name'],
        'JLE' => ['name'],
        'JG' => ['name'],
        'JGE' => ['name'],
        'PROC' => ['name'],
        //служебные
        'EXEC' => ['name'],
        'MOV' => ['name', 'mixed'],
        'STOP' => [],
        'CMP' => ['mixed', 'mixed'],
        'ENDP' => [],
        'SUB' => ['name', 'mixed'],
        'ADD' => ['name', 'mixed'],
        'PUSH' => ['mixed'],
        'POP' => ['mixed']
    ];

    private static function validate_command($name, $args)
    {
        if (!isset(self::$command_prototypes[$name])) {
            throw new \Exception("unknown command '$name'");
        }
        $prototype = self::$command_prototypes[$name];
        if (count($prototype) != count($args)) {
            throw new \Exception(
                "$name expected " .
                count($prototype) .
                " arguments, passed " .
                count($args)
            );
        }
        for ($i = 0; $i < count($args); $i++) {
            switch ($prototype[$i]) {
                case 'name':
                    $pattern = (strlen($args[$i])>1)?'/^[a-zA-Z][\w\-]*[\w]$/':'/[a-zA-Z]/';
                    break;
                case 'number':
                    $pattern = '/^\d+$/';
                    break;
                default:
                    $pattern = '/.*/';
                    break;
            }
            if (preg_match($pattern, $args[$i]) === 0) {
                throw new \Exception(
                    "$name missing argument " .
                    "'{$args[$i]}', expected " .
                    "<b>&lt;{$prototype[$i]}&gt;</b>"
                );
            }
        }
    }

    /**
     * parse command, read type and arguments
     * @param $line
     */
    private static function read_command($line)
    {
        preg_match('/\w+/', $line, $matches);
        $name = $matches[0];
        $substring = substr($line, strpos($line, $name) + strlen($name));
        $substring = trim($substring);
        if (($substring !== false) && ($substring !== '')) {
            $args = explode(',', $substring);
        } else {
            $args = [];
        }

        for ($i = 0; $i < count($args); $i++) {
            $args[$i] = trim($args[$i]);
        }

        self::validate_command($name, $args);

        array_push(self::$commands, [$name, $args, self::$labels]);

        self::$labels = [];
    }

    private static function remove_comment(&$line)
    {
        $index = strpos($line, ';');
        if ($index !== false) {
            $line = trim(substr($line, 0 , $index));
        }
    }

    /**
     * read label
     * @param $line
     * @return string
     * @throws \Exception
     */
    private static function read_label(&$line)
    {
        $index = strpos($line, ':');
        if ($index !== false) {
            $label = trim(substr($line, 0 , $index));
            $line = trim(substr($line, $index+1));
            if ($label === '') {
                throw new \Exception("unexpected ':' on position $index");
            }
            if (preg_match('/^[a-zA-Z][\w\-]*[\w]$/', $label) === 0) {
                throw new \Exception("incorrect label '$label'");
            }
        } else {
            $label = false;
        }
        return $label;
    }

    /**
     * @return mixed
     */
    public static function get_commands()
    {
        return self::$commands;
    }
    /**
     * @return mixed
     */
    public static function get_labels()
    {
        return self::$labels;
    }

    public static function parse($filename, $label_suffix = '')
    {
        self::$commands = [];
        self::$labels = [];
        self::$labels_map = [];
        self::$handle = fopen(DIR_LOGICAL_COMPONENTS."/$filename", 'r');
        $index = 0;
        $command_expect = false;
        if (self::$handle) {
            while (($buffer = fgets(self::$handle)) !== false) {
                $index++;
                try {
                    if ($buffer === "\r\n") {//empty string
                        continue;
                    }
                    $buffer = trim($buffer);
                    self::remove_comment($buffer);
                    $label = self::read_label($buffer);
                    if ($label !== false) {
                        array_push(self::$labels, $label.$label_suffix);
                        array_push(self::$labels_map, $label);
                        $command_expect = true;
                    }
                    if ($buffer !== '') {
                        if ($command_expect === true) {
                            $command_expect = false;
                        }
                        self::read_command($buffer);
                    }
                } catch (\Exception $e) {
                    throw new \Exception("missing interpretation on line $index", 0, $e);
                }
            }
            if (!feof(self::$handle)) {
                throw new \Exception("Error: unexpected fgets() fail");
            }
            fclose(self::$handle);
            //change name labels in jump commands if this require
            for ($i = 0; $i < count(self::$commands); $i++) {
                $name = self::$commands[$i][0];
                if ($name[0] === 'J') {// jump command
                    $args = &self::$commands[$i][1];
                    if (in_array($args[0], self::$labels_map) !== false) {
                        $args[0] .= $label_suffix;
                    }
                }
            }
            for ($i = 0; $i < count(self::$commands); $i++) {
                $name = self::$commands[$i][0];
                $args = self::$commands[$i][1];
                $labels = self::$commands[$i][2];
                Translator::push($name, $args, $labels);
            }
            for ($i = 0; $i < count(self::$labels); $i++) {
                Translator::label(self::$labels[$i]);
            }
        } else {
            throw new \Exception("logic component not found");
        }
    }
}

















