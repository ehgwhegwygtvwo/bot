<?php

namespace db;

/**
 * Class Token
 *
 * @property integer $id
 * @property string $token
 * @property string $group_id
 */
class Token extends DataBase {

    /**
     * Token constructor.
     * @param string $token
     * @param string $group_id
     */
    public function __construct($token, $group_id)
    {
        $this->token = $token;
        $this->group_id = $group_id;
    }

    /**
     * @param Token $token
     * @return bool|int
     */
    public static function save($token)
    {
        global $db;
        if (self::token_exists($token->token)){
            return false;
        }
        $query = "INSERT INTO `deb_token` (`token`, `group_id`) VALUES ('{?}', '{?}')";
        return $db->query($query, [ $token->token, $token->group_id ]);
    }

    /**
     * Check token exists
     *
     * @param $token
     * @return bool
     */
    public static function token_exists($token)
    {
        global $db;
        return $db->selectRow("SELECT * FROM `deb_token` WHERE `token` = '{?}'", [ $token ]);
    }

    /**
     * @param $group_id
     * @return bool|Token
     */
    public static function get_token_by_group_id($group_id)
    {
        global $db;
        return $db->selectRow("SELECT * FROM `deb_token` WHERE `group_id` = '{?}'", [ $group_id ]);
    }

    /**
     * @param $id
     * @return false|Token
     */
    public static function get_token_by_id($id)
    {
        global $db;
        return $db->selectRow("SELECT * FROM `deb_token` WHERE `id` = '{?}'", [ $id ]);
    }

    /**
     * @param $token
     * @return array|bool|false|Owner|stdClass|Token
     */
    public static function get_token_id($token)
    {
        global $db;
        return $db->selectRow("SELECT * FROM `deb_token` WHERE `token` = '{?}'", [ $token ]);
    }
}