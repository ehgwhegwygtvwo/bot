<?php

namespace db;

class CacheMenu extends DataBase
{

    /**
     * update cache menu
     * @param $bot_id
     * @return bool|\Exception|false|int|mixed
     */
    public static function caching_menu($bot_id)
    {
        global $db;
        $bot = Bot::get_bot_by_id($bot_id);
        $group_id = Token::get_token_by_id($bot->token_id)->group_id;
        $query = "INSERT INTO `deb_cache_menu` (`group_id`, `menu`) VALUES ('{?}', '{?}')";

        self::remove_cache_menu($group_id);
        switch ($bot->type_id)
        {
            case MOBIDEL_BOT_TYPE:
                try {
                    $meta = Bot::getMeta($bot_id);
                    $res = \api_crm\MobidelAPI::getRestaurantMenu($meta['restaurantID']);
                    $menu = json_decode($res);
                    $menu = \crm\mobidel\Parse_menu::parse($menu);
                    $result = $db->query($query, [ $group_id, json_encode($menu) ]);
                } catch (\Exception $e) {
                    $result = $e;
                }
                break;
            default:
                $result = false;
                break;
        }
        return $result;
    }

    /**
     * @param $bot_id
     * @return Bot|Owner|\stdClass|Token|Type|false
     */
    public static function remove_cache_menu($group_id)
    {
        global $db;
        $query = "DELETE FROM `deb_cache_menu` WHERE `group_id` = '{?}'";
        return $db->selectRow($query, [ $group_id ]);
    }

    /**
     * get menu by group id
     * @param group_id
     * @return Bot|Owner|\stdClass|Token|Type|false
     */
    public static function get_cache_menu($group_id)
    {
        global $db;
        $query = "SELECT * FROM `deb_cache_menu` WHERE group_id = '{?}'";
        return $db->selectRow($query, [ $group_id ]);
    }

    /**
     * get menu by id
     * @param id
     * @return Bot|Owner|\stdClass|Token|Type|false
     */
    public static function get_cache_menu_by_id($id)
    {
        global $db;
        $query = "SELECT * FROM `deb_cache_menu` WHERE id = '{?}'";
        return $db->selectRow($query, [ $id ]);
    }
}