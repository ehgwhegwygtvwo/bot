<?php

namespace db;

/**
 * This class contain many sql queries for rapid select operations
 */
class Common extends DataBase
{
    /**
     * get all data about group
     * @param $group_id
     * @return false|int
     */
    public static function fetch_group_data($group_id)
    {
        global $db;
        $query = "
            SELECT
                deb_group.group_id,
                deb_token.token,
                deb_bot.id AS bot_id,
                deb_cache_menu.menu
            FROM
                deb_group
            LEFT JOIN
                deb_token
                    ON deb_group.group_id = deb_token.group_id
            LEFT JOIN
                deb_bot
                    ON deb_token.id = deb_bot.token_id
            LEFT JOIN
                deb_cache_menu
                    ON deb_group.group_id = deb_cache_menu.group_id
            WHERE 
                deb_group.group_id = '{?}'
        ";
        return $db->query($query, $group_id);
    }

    /**
     * @param $owner_phone
     * @return array|bool
     */
    public static function fetch_bots_data($owner_phone)
    {
        global $db;
        $query = "
            SELECT
                deb_bot.id AS 'id',
                deb_group.group_id AS 'group_id',
                deb_bot.payment AS 'payment',
                deb_bot.active AS 'active'
            FROM
                deb_owner
                    LEFT JOIN
                        deb_group
                    ON
                        deb_owner.id = deb_group.owner_id
                    LEFT JOIN
                        deb_token
                    ON
                        deb_group.group_id = deb_token.group_id
                    LEFT JOIN
                        deb_bot
                    ON
                        deb_token.id = deb_bot.token_id
            WHERE
                phone = '{?}' AND deb_bot.id != ''
        ";
        return $db->select($query, [ $owner_phone ]);
    }

    /**
     * get all payloads for execution bot
     * @param $group_id
     * @param $vk_user_id
     * @return Bot|Owner|Token|Type|false|\stdClass
     */
    public static function fetch_chat_payloads($group_id, $vk_user_id)
    {
        global $db;
        $query = "
            SELECT
                deb_group.group_id AS group_id,
                deb_bot.id AS bot_id,
                deb_bot.type_id AS type_id,
                deb_bot.commands AS commands,
                deb_bot.labels AS labels,
                deb_cache_menu.menu AS menu,
                deb_state.state AS state,
                deb_token.token AS token,
                GROUP_CONCAT(deb_meta.key) AS meta_keys,
                GROUP_CONCAT(deb_meta.value) AS meta_values
            FROM
                deb_group
            LEFT JOIN
                deb_token
                    ON deb_group.group_id = deb_token.group_id
            LEFT JOIN
                deb_bot
                    ON deb_token.id = deb_bot.token_id
            LEFT JOIN
                deb_cache_menu
                    ON deb_group.group_id = deb_cache_menu.group_id
            LEFT JOIN
                deb_state
                    ON deb_state.vk_user_id = '{?}'
            LEFT JOIN
                deb_meta
                    ON deb_meta.bot_id = deb_bot.id
            WHERE deb_group.group_id = '{?}'
            GROUP BY bot_id, menu, state, token
        ";
        $row = $db->selectRow($query, [ $vk_user_id, $group_id]);
        $keys = explode(',', $row->meta_keys);
        $values = explode(',', $row->meta_values);
        $row->meta = [];
        for ($i = 0; $i < count($keys); $i++) {
            $row->meta[$keys[$i]] = $values[$i];
        }
        if ($row->state) {
            $row->state = json_decode($row->state, true);
        }
        return $row;
    }

    /**
     * get all tokens for for owner
     * @param $owner_phone
     * @return bool|false|int|mixed
     */
    public static function fetch_all_tokens($owner_phone)
    {
        global $db;
        $query = "
            SELECT
                deb_group.group_id AS group_id,
                deb_token.id AS token_id,
                deb_token.token AS token
            FROM
                deb_owner
                LEFT JOIN
                    deb_group
                ON
                    deb_owner.id = deb_group.owner_id
                LEFT JOIN
                    deb_token
                ON
                    deb_group.group_id = deb_token.group_id
            WHERE
                deb_owner.phone = '{?}'
        ";
        return $db->select($query, [$owner_phone]);
    }
}