<?php

namespace db;

/**
* Manages states of users
*
* "State" it dialog metadata of user and bot.
* It's system provide save context of dialog.
*/
class State extends DataBase {
    /**
     * @param $bot_id
     * @param $vk_user_id
     * @return array|State
     */
    public static function get_state($bot_id, $vk_user_id)
    {
        $state = json_decode(self::select_state($bot_id, $vk_user_id), true);
        if (!$state) {
            $bot = \db\Bot::get_bot_by_id($bot_id);
            $state = self::init($bot->type_id, $bot_id, $vk_user_id);
        }
        return $state;
    }

    /**
     * Create new state
     * @param integer $type_id
     * @param integer $bot_id
     * @param integer $vk_user_id
     * @return array|false
     */
    private static function init($type_id, $bot_id, $vk_user_id)
    {
        global $db, $_PROTOTYPES;
        $type = Type::get_type_by_id($type_id)->type;
        $state = [];

        foreach ($_PROTOTYPES[$type] as $name => $param){
            $state[$name] = $param['default'];
        }

        $query = "INSERT INTO `deb_state` (`bot_id`, `vk_user_id`, `state`) VALUES ('{?}', '{?}', '{?}')";

        if ($db->query($query, [ $bot_id, $vk_user_id, json_encode($state) ])) {
            return $state;
        } else {
            return false;
        }
    }

    /**
     * Remove all states for bot by bot id
     * @param string $bot_id
     * @return bool|int
     */
    public static function drop_states($bot_id)
    {
        global $db;
        $query = "DELETE FROM `deb_state` WHERE `bot_id` = '{?}'";
        return $db->query($query, [ $bot_id ]);
    }

    /**
     * drop state for one user
     * @param $bot_id
     * @param $vk_user_id
     * @return false|int
     */
    public static function dropState($bot_id, $vk_user_id)
    {
        global $db;
        $query = "DELETE FROM `deb_state` WHERE `bot_id` = '{?}' AND `vk_user_id` = '{?}'";
        return $db->query($query, [ $bot_id, $vk_user_id ]);
    }
    /**
     * drop all states for one user
     * @param $vk_user_id
     * @return false|int
     */
    public static function dropUserStates($vk_user_id)
    {
        global $db;
        $query = "DELETE FROM `deb_state` WHERE AND `vk_user_id` = '{?}'";
        return $db->query($query, [ $vk_user_id ]);
    }

    /**
     * Load state if it exists or init new state
     * @param $bot_id
     * @param $vk_user_id
     * @return bool
     */
    public static function select_state($bot_id, $vk_user_id)
    {
        global $db;
        $query = "SELECT `state` FROM `deb_state` WHERE `bot_id` = '{?}' AND `vk_user_id` = '{?}'";
        return $db->selectCell($query, [ $bot_id, $vk_user_id ]);
    }

    /**
     * Save state
     */
    public static function save($bot_id, $vk_user_id, $state)
    {
        global $db;
        $db->query(
            "UPDATE deb_state SET state = '{?}' WHERE bot_id = '{?}' AND vk_user_id = '{?}'",
            [ json_encode($state), $bot_id, $vk_user_id ]
        );
    }

}