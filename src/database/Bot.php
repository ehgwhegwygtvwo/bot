<?php

namespace db;

/**
 * Class Bot
 * @property string commands
 * @property string labels
 * @property integer token_id
 * @property integer id
 * @property integer type_id
 * @property array meta
 * @property \FunctionsInterface api
 */
class Bot extends DataBase {
    /**
     * Bot constructor.
     * @param int $token_id
     */
    public function __construct($token_id)
    {
        //set fields from deb_bot
        $bot = self::get_bot_by_token_id($token_id);
        foreach (get_object_vars($bot) as $key => $value) {
            $this->$key = $value;
        }
        //set meta
        $meta = Meta::get_meta_by_bot_id($bot->id);
        $this->meta = [];
        for ($i=0; $i < count($meta); $i++) {
            $this->meta[$meta[$i]->key] = $meta[$i]->value;
        }
    }

    /**
     * set suitable api for crm interaction
     */
    public function init_api()
    {
        switch ($this->type_id)
        {
            case MOBIDEL_BOT_TYPE:
                $this->api = new MobidelFunctions($this->meta);
                break;
        }
    }

    public static function get_bot_by_id($bot_id)
    {
        global $db;
        $query = "SELECT * FROM `deb_bot` WHERE `id` = '{?}'";
        return $db->selectRow($query, [ $bot_id ]);
    }

    /**
     * @param $token_id
     * @return false|Bot|\stdClass
     */
    public static function get_bot_by_token_id($token_id)
    {
        global $db;
        return $db->selectRow("SELECT * FROM `deb_bot` WHERE `token_id` = '{?}'", [ $token_id ]);
    }

    /**
     * @return mixed
     */
    public static function getMeta($bot_id)
    {
        return Meta::get_meta_by_bot_id($bot_id);
    }

    /**
     * @return mixed
     */
    public static function getMenu($bot_id)
    {
        return CacheMenu::get_cache_menu($bot_id);
    }

    /**
     * Build bot by token
     * @param $token_id
     * @param $programm
     * @param integer $type constant example MOBIDEL_TYPE_BOT
     * @return false|int return bot id or false
     */
    public static function update($token_id, $programm, $source, $type)
    {
        global $db;
        $params = [
            json_encode($programm['commands']),
            json_encode($programm['labels']),
            $type,
            json_encode($source),
            $token_id
        ];
        $bot = Bot::get_bot_by_token_id($token_id);
        if ($bot) {
            $bot_id = $bot->id;
            $query = "
                UPDATE `deb_bot` 
                SET `commands` = '{?}', `labels` = '{?}', `type_id` = '{?}', `source` = '{?}'
                WHERE `token_id` = '{?}'";
            $db->query($query, $params);
        } else {
            $query = "
                INSERT INTO `deb_bot` (`commands`, `labels`, `type_id`, `source`, `token_id`, `payment`) 
                VALUES ('{?}', '{?}', '{?}', '{?}', '{?}', NOW())
            ";
            $bot_id = $db->query($query, $params);
        }
        return $bot_id;
    }

    /**
     * remove bot by id
     * @param $bot_id
     * @return bool|false|int|mixed
     */
    public static function bot_remove($bot_id)
    {
        global $db;
        $query = "DELETE FROM `deb_bot` WHERE `id` = '{?}'";
        return $db->query($query, [ $bot_id ]);
    }

    /**
     * fetch bot by id
     * @param $bot_id
     * @return Bot
     */
    public static function fetch_bot($bot_id)
    {
        global $db;
        $query = "SELECT * FROM `deb_bot` WHERE `id` = '{?}'";
        return $db->selectRow($query, [ $bot_id ]);
    }
}