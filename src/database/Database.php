<?php

namespace db;

class DataBase {
    private static $db = null; // Единственный экземпляр класса, чтобы не создавать множество подключений
    private $mysqli; // Идентификатор соединения
    private $sym_query = "{?}"; // "Символ значения в запросе"

    /* Получение экземпляра класса. Если он уже существует, то возвращается, если его не было, то создаётся и возвращается (паттерн Singleton) */
    public static function getDB()
    {
        if (self::$db == null){
            self::$db = new DataBase();
        }
        return self::$db;
    }

    /* private-конструктор, подключающийся к базе данных, устанавливающий локаль и кодировку соединения */
    private function __construct()
    {
        $this->mysqli = new \mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
        $this->mysqli->query("SET lc_time_names = 'ru_RU'");
        $this->mysqli->query("SET NAMES 'utf8'");
    }
    /* Вспомогательный метод, который заменяет "символ значения в запросе" на конкретное значение, которое проходит через "функции безопасности" */
    private function getQuery($pattern, $params)
    {
        if ($params) {
            for ($i = 0; $i < count($params); $i++) {
                $pos = strpos($pattern, $this->sym_query);
                if ($pos != false) {
                    $arg = $this->mysqli->real_escape_string($params[$i]);
                    $pattern = substr_replace($pattern, $arg, $pos, strlen($this->sym_query));
                }
            }
        }
//        echo $pattern; // debug
        return $pattern;
    }
    /**
     * Get rows from mysqli_result
     *
     * @param \mysqli_result $result_set
     * @return array list of objects
     */
    private function resultSetToArray($result_set)
    {
        $array = array();
        while (($row = $result_set->fetch_object()) != false) {
            $array[] = $row;
        }
        return $array;
    }

    /**
     * Simple sql select return array of rows
     *
     * @param string $query
     * @param bool $params
     * @return array|bool
     */
    protected function select($query, $params = false)
    {
        $query = $this->getQuery($query, $params);
        $result_set = $this->mysqli->query($query);
        if (!$result_set) return false;
        return $this->resultSetToArray($result_set);
    }
    /**
     * Select one row
     *
     * similar findOne form mongoose
     *
     * @param string $pattern
     * @param bool|array $params
     * @return \stdClass|false|Owner|Token|Bot|Type|Group
     */
    protected function selectRow($pattern, $params = false)
    {
        $query = $this->getQuery($pattern, $params);
        $result_set = $this->mysqli->query($query);
        if ($result_set) {
            if ($result_set->num_rows != 1) {
                return false;
            } else {
                return $result_set->fetch_object();
            }
        } else {
            return false;
        }
    }

    /* SELECT-метод, возвращающий значение из конкретной ячейки */
    protected function selectCell($pattern, $params = false)
    {
        $query = $this->getQuery($pattern, $params);
        $result_set = $this->mysqli->query($query);
        if ((!$result_set) || ($result_set->num_rows < 1)) {
            return false;
        } else {
            $arr = array_values($result_set->fetch_assoc());
            return $arr[0];
        }
    }
    /**
     * For INSERT, UPDATE and DELETE methods
     *
     * @param string $query
     * @param bool|array $params
     * @return false|integer
     */
    protected function query($query, $params = false)
    {
        $query = $this->getQuery($query, $params);
        $success = $this->mysqli->query($query);
        if ($success) {
            if ($this->mysqli->insert_id === 0){
                return true;
            }
            else return $this->mysqli->insert_id;
      }
      else return false;
    }

    /* При уничтожении объекта закрывается соединение с базой данных */
    public function __destruct()
    {
        if ($this->mysqli) $this->mysqli->close();
    }
}

$db = DataBase::getDB();