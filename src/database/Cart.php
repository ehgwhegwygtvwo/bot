<?php

namespace db;

//CART FIELD JSON STRUCTURE
//[
//	<idArticle> => [
//    "quantity" => "",
//    "semiproducts" => [
//            <idArticle> => <quantity>
//        ]
//	]
//]

class Cart extends DataBase {
    private static function insert($articles, $bot_id, $vk_user_id)
    {
        global $db;
        $query = "INSERT INTO `deb_cart` (`articles`, `vk_user_id`, `bot_id`) VALUES ('{?}', '{?}', '{?}')";
        $db->query($query, [json_encode($articles), $vk_user_id, $bot_id]);

    }
    private static function update($articles, $bot_id, $vk_user_id)
    {
        global $db;
        $query = "UPDATE `deb_cart` SET `articles` = '{?}' WHERE `vk_user_id` = '{?}' AND `bot_id` = '{?}'";
        $db->query($query, [json_encode($articles), $vk_user_id, $bot_id]);
    }

    /**
     * remove if $vk_user_id passed remove cart for this user else remove all carts by bot id
     * @param $bot_id
     * @param string| $vk_user_id
     */
    private static function remove($bot_id, $vk_user_id = '')
    {
        global $db;
        if ($vk_user_id === '') {
            $query = "DELETE FROM `deb_cart` WHERE `bot_id` = '{?}' AND `vk_user_id` = '{?}'";
        } else {
            $query = "DELETE FROM `deb_cart` WHERE `bot_id` = '{?}'";
        }
        $db->query($query, [$bot_id, $vk_user_id]);
    }

    /**
     * get array of articles
     * @param $bot_id
     * @param $vk_user_id
     * @return array
     */
    public static function fetch_articles($bot_id, $vk_user_id)
    {
        global $db;
        $query = "SELECT `articles` FROM deb_cart WHERE bot_id = '{?}' AND vk_user_id = '{?}'";
        $articles = $db->selectCell($query, [$bot_id, $vk_user_id]);
        if ($articles === false) {
            $articles = false;
        } elseif (!is_array($articles = json_decode($articles, true))) {
            //TODO:error write to log
            self::remove($bot_id, $vk_user_id);
            $articles = false;
        }
        return $articles;
    }

    /**
     * get article by id. If article do not exists return false
     * @param $product_id
     * @param $bot_id
     * @param $vk_user_id
     * @return false|object
     */
    public static function get_article($product_id, $bot_id, $vk_user_id)
    {
        $articles = self::fetch_articles($bot_id, $vk_user_id);
        if (isset($articles[$product_id])) {
            $article = $articles[$product_id];
        } else {
            $article = false;
        }
        return $article;
    }

    public static function save_articles($articles, $bot_id, $vk_user_id)
    {
        self::remove($bot_id, $vk_user_id);
        self::insert($articles, $bot_id, $vk_user_id);
    }

    public static function drop($bot_id, $vk_user_id)
    {
        self::remove($bot_id, $vk_user_id);
    }

    /**
     * add one product in cart
     * @param $bot_id
     * @param $vk_user_id
     * @param $product_id
     * @return number
     */
    public static function inc_product($bot_id, $vk_user_id, $product_id)
    {
        $articles = self::fetch_articles($bot_id, $vk_user_id);
        if ($articles === false) {
            $articles = [];
            $articles[$product_id] = [
                'quantity' => 1,
                'semiproducts' => []
            ];
            self::insert($articles, $bot_id, $vk_user_id);
        } else {
            if (isset($articles[$product_id])) {
                $articles[$product_id]['quantity'] += 1;
            } else {
                $articles[$product_id] = [
                    'quantity' => 1,
                    'semiproducts' => []
                ];
            }
            self::update($articles, $bot_id, $vk_user_id);
        }
        return $articles[$product_id]['quantity'];
    }
    /**
     * remove one product from cart
     * @param $bot_id
     * @param $vk_user_id
     * @param $product_id
     * @return number
     */
    public static function dec_product($bot_id, $vk_user_id, $product_id)
    {
        $articles = self::fetch_articles($bot_id, $vk_user_id);
        $quantity = 0;
        if ($articles !== false) {
            if ((isset($articles[$product_id])) && ($articles[$product_id]['quantity'] > 0)) {
                $articles[$product_id]['quantity'] -= 1;
                self::update($articles, $bot_id, $vk_user_id);
                $quantity = $articles[$product_id]['quantity'];
            }
        }
        return $quantity;
    }
    /**
     * remove product from cart
     * @param $bot_id
     * @param $vk_user_id
     * @param $product_id
     */
    public static function remove_product($bot_id, $vk_user_id, $product_id)
    {
        $articles = self::fetch_articles($bot_id, $vk_user_id);
        if (($articles !== false) && (isset($articles[$product_id]))) {
            unset($articles[$product_id]);
            self::update($articles, $bot_id, $vk_user_id);
        }
    }

    /**
     * add one semiproduct in product
     * @param $bot_id
     * @param $vk_user_id
     * @param $product_id
     * @param $semiproduct_id
     * @return int|bool
     */
    public static function inc_semiproduct($bot_id, $vk_user_id, $product_id, $semiproduct_id)
    {
        $articles = self::fetch_articles($bot_id, $vk_user_id);
        if (isset($articles[$product_id])) {
            if (isset($articles[$product_id]['semiproducts'][$semiproduct_id])) {
                $articles[$product_id]['semiproducts'][$semiproduct_id]['quantity'] += 1;
            } else {
                $articles[$product_id]['semiproducts'][$semiproduct_id] = ['quantity' => 1];
            }
            self::update($articles, $bot_id, $vk_user_id);
            return $articles[$product_id]['semiproducts'][$semiproduct_id]['quantity'];
        }
        return false;
    }
    /**
     * remove one semiproduct from product
     * @param $bot_id
     * @param $vk_user_id
     * @param $product_id
     * @param $semiproduct_id
     * @return int|bool
     */
    public static function dec_semiproduct($bot_id, $vk_user_id, $product_id, $semiproduct_id)
    {
        $articles = self::fetch_articles($bot_id, $vk_user_id);
        if ((isset($articles[$product_id])) &&
            (isset($articles[$product_id]['semiproducts'][$semiproduct_id])) &&
            ($articles[$product_id]['semiproducts'][$semiproduct_id]['quantity'] > 0)) {
            $articles[$product_id]['semiproducts'][$semiproduct_id]['quantity'] -= 1;
            self::update($articles, $bot_id, $vk_user_id);
            return $articles[$product_id]['semiproducts'][$semiproduct_id]['quantity'];
        }
        return false;
    }
    /**
     * remove semiproduct from product
     * @param $bot_id
     * @param $vk_user_id
     * @param $product_id
     * @param $semiproduct_id
     */
    public static function remove_semiproduct($bot_id, $vk_user_id, $product_id, $semiproduct_id)
    {
        $articles = self::fetch_articles($bot_id, $vk_user_id);
        if ((isset($articles[$product_id])) &&
            (isset($articles[$product_id]['semiproducts'][$semiproduct_id]))
        ){
            unset($articles[$product_id]['semiproducts'][$semiproduct_id]);
            self::update($articles, $bot_id, $vk_user_id);
        }
    }

    /**
     * shell for remove method
     * @param $bot_id
     * @param $vk_user_id
     */
    public static function dropUserState($bot_id, $vk_user_id)
    {
        self::remove($bot_id, $vk_user_id);
    }
}



















