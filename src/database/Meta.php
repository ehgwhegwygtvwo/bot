<?php

namespace db;

class Meta extends DataBase
{
    /**
     * parser meta result from database
     * @param array $meta
     * @return array
     */
    private static function parse_meta($meta)
    {
        $arr = [];
        for ($i = 0; $i < count($meta);  $i++)
        {
            $arr[$meta[$i]->key] = $meta[$i]->value;
        }
        return $arr;
    }

    /**
     * TODO: in else branch replace `$meta = [];` on $meta = false;
     * get bot metadata by bot id
     * @param string $bot_id
     * @return array
     */
    public static function get_meta_by_bot_id($bot_id)
    {
        global $db;
        $query = "SELECT `key`, `value` FROM deb_meta WHERE bot_id='{?}'";
        $meta = $db->select($query, [ $bot_id ]);
        if ($meta) {
            $meta = self::parse_meta($meta);
        } else {
            $meta = [];
        }
        return $meta;
    }

    /**
     * remove meta by bot id
     * @param $bot_id
     * @return false|int
     */
    public static function drop_meta($bot_id)
    {
        global $db;
        $query = "DELETE FROM `deb_meta` WHERE `bot_id` = '{?}' ";
        return $db->query($query, [ $bot_id ]);
    }

    /**
     * save meta data
     * @param array $meta
     */
    public static function update_meta($bot_id, $meta)
    {
        global $db;
        $query_insert = 'INSERT INTO `deb_meta` (`bot_id`, `key`, `value`) VALUES';
        $query_delete = "DELETE FROM `deb_meta` WHERE `bot_id` = '$bot_id' AND (";
        for ($i = 0; $i < count($meta); $i++)
        {
            $key = $meta[$i]['key'];
            $value = $meta[$i]['value'];
            if ($i){
                $query_insert .= ", ";
                $query_delete .= " OR ";
            }
            $query_insert .= "('$bot_id', '$key', '$value')";
            $query_delete .= "`key` = '$key'";
        }
        $query_delete .= ')';
        $db->query($query_delete);
        return $db->query($query_insert);
    }
}