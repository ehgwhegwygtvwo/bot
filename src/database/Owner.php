<?php

namespace db;

/**
 * Class Owner
 *
 * @property integer $id
 * @property string $phone
 */
class Owner extends DataBase {
    /**
     * Save owner
     * @param Owner $owner
     * @return bool|int
     */
    public static function save($owner)
    {
        global $db;
        if (!self::owner_isset($owner)) {
            $query = "INSERT INTO `deb_owner` (`phone`) VALUES ('{?}')";
            $id = $db->query($query, [ $owner->phone ]);
            $owner->id = $id;
            return $id;
        }
        return false;
    }

    /**
     * Check isset owner by phone
     *
     * @param Owner $owner
     * @return bool|Owner|\stdClass
     */
    public static function owner_isset($owner)
    {
        global $db;
        $query = "SELECT * FROM deb_owner WHERE phone = '{?}'";
        return $db->selectRow($query, [ $owner->phone ]);
    }

    /**
     * Change phone for user
     *
     * @param Owner $owner
     * @param string $new_phone
     * @return bool
     */
    public static function change_phone($owner, $new_phone)
    {
        global $db;
        $old_phone = $owner->phone;
        $query = "UPDATE deb_owner SET phone = '{?}' WHERE phone = '{?}'";
        if ($db->query($query, [ $new_phone, $old_phone])) {
            $owner->phone = $new_phone;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get owner by phone
     *
     * @param string $phone
     * @return Owner|false
     */
    public static function get_owner_by_phone($phone)
    {
        global $db;
        $query = "SELECT * FROM `deb_owner` WHERE `phone` = '{?}'";
        return $db->selectRow($query, [ $phone ]);
    }
}
