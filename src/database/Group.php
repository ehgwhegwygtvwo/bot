<?php

namespace db;

/**
 * @property integer $group_id
 * @property string $server_confirm_code
 * @property integer $owner_id
 * @property bool $confirmed
 */
class Group extends DataBase {
    /**
     * @param $group_id
     * @param $owner_id
     * @param $server_confirm_code
     * @return false|int
     * @throws \Exception
     */
    public static function group_add($group_id, $owner_id, $server_confirm_code)
    {
        if (self::group_exists($group_id)) {
            throw new \Exception('group already exists', -1);
        }
        return self::group_insert($owner_id, $group_id, $server_confirm_code);
    }

    /**
     * @param $owner_id
     * @param $group_id
     * @param $server_confirm_code
     * @return false|int
     */
    private static function group_insert($owner_id, $group_id, $server_confirm_code)
    {
        global $db;
        $query = "
            INSERT INTO `deb_group` (`owner_id`, `group_id`, server_confirm_code) 
            VALUES ('{?}', '{?}', '{?}')
        ";
        $params = [
            $owner_id,
            $group_id,
            $server_confirm_code
        ];
        return $db->query($query, $params);
    }

    /**
     * @param Group $group
     * @return false|Group
     */
    private static function group_exists($group_id)
    {
        global $db;
        $query = "SELECT * FROM `deb_group` WHERE `group_id` = '{?}'";
        return $db->selectRow($query, [ $group_id ]);
    }

    /**
     * Get vk group by group vk id
     *
     * @param $group_id
     * @return Group
     */
    public static function get_group_by_id($group_id)
    {
        global $db;
        $query = "SELECT * FROM `deb_group` WHERE `group_id` = '{?}'";
        return $db->selectRow($query, [ $group_id ]);
    }

    /**
     * Set confirmed status for group
     *
     * @param $group_id
     * @param $confirmed
     * @return bool|Owner|/stdClass
     */
    public static function set_confirmed($group_id, $confirmed = 1)
    {
        global $db;
        $query = "UPDATE `deb_group` SET `confirmed` = '{?}' WHERE `group_id` = '{?}'";
        return $db->selectRow($query, [ $confirmed, $group_id ]);
    }

    /**
     * @param $group
     * @return array|bool|Bot|Owner|Token|Type|false|\stdClass
     */
    public static function set_confirmed_code($group_id, $server_confirm_code)
    {
        global $db;
        $query = "UPDATE `deb_group` SET `server_confirm_code` = '{?}' WHERE `group_id` = '{?}'";
        return $db->selectRow($query, [ $server_confirm_code, $group_id ]);
    }
}
