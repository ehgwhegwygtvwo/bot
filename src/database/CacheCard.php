<?php

namespace db;

class CacheCard extends DataBase
{
    /**
     * @param $group_id
     * @param $cards
     * @return false|int
     */
    private static function insert($group_id, $cards)
{
    global $db;
    $query = "INSERT INTO `deb_cache_card` (`group_id`, `cards`) VALUES ('{?}', '{?}')";
    return $db->query($query, [ $group_id, $cards ]);
}
    /**
     * if cache for group with group id isset return true else false
     * @param $group_id
     * @return bool
     */
    private static function cache_isset($group_id)
    {
        global $db;
        $query = "SELECT * FROM `deb_cache_card` WHERE `group_id` = '{?}'";
        $result = $db->select($query, [ $group_id ]);
        if (count($result)) {
            return true;
        }
        return false;
    }
    /**
     * @param $group_id
     * @param $cards
     * @return false|int
     */
    private static function update($group_id, $cards)
    {
        global $db;
        $query = "UPDATE `deb_cache_card` SET `cards` ='{?}' WHERE `group_id` = '{?}'";
        return $db->query($query, [ $cards, $group_id ]);
    }

    /**
     * update cache cards
     * @param $bot_id
     * @return bool|\Exception|false|int|mixed
     */
    public static function update_card($bot_id, $cards)
    {
        global $db;
        $bot = Bot::get_bot_by_id($bot_id);
        $group_id = Token::get_token_by_id($bot->token_id)->group_id;

        if (self::cache_isset($group_id)) {
            $result = self::update($group_id, json_encode($cards));
        } else {
            if ($cards == '') {
                switch ($bot->type_id)
                {
                    case MOBIDEL_BOT_TYPE:
                        $cards = '{"product_id":[],"photo_id":[]}';
                        break;
                }
            }
            if (is_object($cards) || is_array($cards)) {
                $cards = json_encode($cards);
            }
            $result = self::insert($group_id, $cards);
        }

        return $result;
    }
    /**
     * @param $group_id
     * @return \stdClass
     */
    public static function fetch_cards($group_id)
    {
        global $db;
        $token = Token::get_token_by_group_id($group_id);
        $bot = Bot::get_bot_by_token_id($token->id);
        $query = "SELECT `cards` FROM `deb_cache_card` WHERE `group_id` = '{?}'";
        $cards = $db->selectCell($query, [ $group_id ]);
        if (($cards === false) || (!($cards = json_decode($cards)))) {
            switch ($bot->type_id)
            {
                case MOBIDEL_BOT_TYPE:
                    $cards = new \stdClass();
                    $cards->product_id = [];
                    $cards->photo_id = [];
                    break;
            }
        }
        return $cards;
    }
    /**
     * @param $group_id
     * @return Bot|Owner|Token|Type|false|\stdClass
     */
    public static function remove_cache_card($group_id)
    {
        global $db;
        $query = "DELETE FROM `deb_cache_card` WHERE `group_id` = '{?}'";
        return $db->selectRow($query, [ $group_id ]);
    }
}