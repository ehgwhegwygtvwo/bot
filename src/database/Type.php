<?php

namespace db;

/**
 * Class Type
 * @property string $id
 * @property string $type
 * @property string $price
 */
class Type extends DataBase {
/**
 * @param $id
 * @return Type|false
 */
public static function get_type_by_id($id)
    {
        global $db;
        return $db->selectRow("SELECT * FROM `deb_type` WHERE `id` = '{?}'", [ $id ]);
    }
}