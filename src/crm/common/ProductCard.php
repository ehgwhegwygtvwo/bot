<?php
/**
 * Created by PhpStorm.
 * User: Air
 * Date: 08.10.2018
 * Time: 14:41
 */

namespace crm\common;

class ProductCard
{
    /**
     * @var 0 $offsettop current offset content from top card border
     * @var null $card product card resource
     * @var null $thumb product card thumb resource
     * @var null $thumb_imagesize result of getimagesize for thumb
     */
    private
        $card = null,
        $thumb = null,
        $thumb_imagesize = null,
        $width = 0,
        $height = 0,
        $thumb_height = 0,
        $offsettop = 0,
        $colors = [];
    const
        ROOT = ROOT.'/assets/card',
        COLOR = [
            "background"    => [255, 255, 255],
            "index"         => [255, 255, 255],
            "header"        => [59,  72,  92 ],
            "description"   => [71,  83,  102],
            "price"         => [52,  158, 255],
            "old_price"     => [133, 197, 255],
            "weight"        => [59,  72,  92 ],
            "border"        => [237, 237, 237]
        ],
        FONT = [
            "header" => [
                "size" => 32,
                "line_height" => 32,
                "uri" => self::ROOT.'/fonts/clear-sans/clear-sans-bold.ttf'
            ],
            "description" => [
                "size" => 21,
                "line_height" => 40,
                "uri" => self::ROOT.'/fonts/clear-sans/clear-sans-regular.ttf'
            ],
            "price" => [
                "size" => 45,
                "line_height" => 45,
                "uri" => self::ROOT.'/fonts/clear-sans/clear-sans-bold.ttf'
            ],
            "old_price" => [
                "size" => 33,
                "line_height" => 33,
                "uri" => self::ROOT.'/fonts/clear-sans/clear-sans-bold.ttf'
            ],
            "weight" => [
                "size" => 33,
                "line_height" => 33,
                "uri" => self::ROOT.'/fonts/clear-sans/clear-sans-bold.ttf'
            ],
            "index" => [
                "size" => 60,
                "line_height" => 60,
                "uri" => self::ROOT.'/fonts/clear-sans/clear-sans.ttf'
            ]
        ],
        MARGIN = [
            "header" => [
                "top" => 47,
                "left" => 0,
            ],
            "description" => [
                "top" => 10,
                "left" => 80,
            ],
            "price" => [
                "left" => 80,
                "bottom" => 70,
            ],
            "old_price" => [
                "left" => 17,
                "bottom" => 65,
            ],
            "weight" => [
                "right" => 70,
            ]
        ],
        SIZE = [
            "width"         => 1000,
            "height"        => 1000,
            "thumb_height"  => 600
        ],
        VALID_IMAGE_TYPE = [IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG, IMAGETYPE_BMP],
        MATERIALS = [
            'index_background_url' => self::ROOT.'/materials/index_background.png',
            'action_overlay_url' =>  self::ROOT.'/materials/action_overlay.png'
        ],
        DEFAULT_URI_THUMB_IMAGE = self::ROOT.'/default_thumb.jpg',
        SAVE_PATH = './';

    /**
     * ProductCard constructor.
     */
    public function __construct()
    {
        $this->width        = self::SIZE['width'];
        $this->height       = self::SIZE['height'];
        $this->card         = imagecreatetruecolor($this->width, $this->height);
        $this->thumb_height = self::SIZE['thumb_height'];
        $this->init_colors();
    }

    /**
     * initialize colors from $colors array in card source
     * @param $colors
     */
    private function init_colors()
    {
        foreach (self::COLOR as $key => $color) {
            $this->colors[$key] = imagecolorallocate($this->card, $color[0], $color[1], $color[2]);
        }
    }

    /**
     * load thumb from url
     * @param string $url url of a thumb
     * @return bool
     */
    private function get_thumb($url)
    {
        $size = getimagesize($url);
        if (in_array($size[2], self::VALID_IMAGE_TYPE)) {
            switch ($size['mime']) {
                case 'image/jpeg':
                    $this->thumb = imagecreatefromjpeg($url);
                    break;
                case 'image/png':
                    $this->thumb = imagecreatefrompng($url);
                    break;
                default:
                    $this->thumb = false;
                    break;
            }
            $this->thumb_imagesize = $size;
            return 1;
        }
        return 0;
    }

    /**
     * overlay thumb image on product card
     */
    private function thumb_overlay()
    {
        $ratio = $this->thumb_imagesize[1] / $this->thumb_height;
        $new_width = round($this->thumb_imagesize[0] / $ratio);
        $dst_x = -($new_width - $this->width) / 2;
        imagecopyresampled(
            $this->card,
            $this->thumb,
            $dst_x, 1,
            0, 0,
            $new_width, $this->thumb_height,
            $this->thumb_imagesize[0], $this->thumb_imagesize[1]
        );
        $this->offsettop += $this->thumb_height;
    }

    private function draw_header($header)
    {
        $this->offsettop += self::MARGIN['header']['top'];

        $box =imageftbbox(self::FONT['header']['size'], 0, self::FONT['header']['uri'], $header);
        $xr = abs(max($box[2], $box[4]));
        $offset_left = intval((self::SIZE['width'] - $xr)/2);
        imagettftext(
            $this->card,
            self::FONT['header']['size'],
            0,
            $offset_left,
            $this->offsettop + self::FONT['header']['line_height'],
            $this->colors['header'],
            self::FONT['header']['uri'],
            $header
        );
        $this->offsettop += self::FONT['header']['line_height'];
    }
    private function draw_description($description)
    {
        $this->offsettop += self::MARGIN['description']['top'];
        $text = explode("\n", wordwrap($description, 107, "\n"));
        //TODO: этот параметер должен замениться расчётом границ коробки текста
        for ($i = 0; $i < count($text); $i += 1) {
            imagettftext(
                $this->card,
                self::FONT['description']['size'],
                0,
                self::MARGIN['description']['left'],
                $this->offsettop + self::FONT['description']['line_height'],
                $this->colors['header'],
                self::FONT['description']['uri'],
                $text[$i]
            );
            $this->offsettop += self::FONT['description']['line_height'];
        }

//        $max_line_length = 30;
//        for ($i = 0; $i < strlen($description); $i += $max_line_length) {
//            $str = mb_substr($description, $i, $max_line_length);
//            imagettftext(
//                $this->card,
//                self::FONT['description']['size'],
//                0,
//                self::MARGIN['header']['left'],
//                $this->offsettop + self::FONT['description']['line_height'],
//                $this->colors['header'],
//                self::FONT['description']['uri'],
//                $str
//            );
//            $this->offsettop += self::FONT['description']['line_height'];
//        }
    }
    private function draw_border()
    {
        $color = $this->colors['border'];
        $im = $this->card;
        $width = self::SIZE['width']-1;
        $height = self::SIZE['height']-1;
        imageline($im, 0, 0, 0, $height, $color);
        imageline($im, 0, 0, $width, 0, $color);
        imageline($im, $width, $height, $width, 0, $color);
        imageline($im, $width, $height, 0, $height, $color);
    }
    private function draw_price($price)
    {
        $this->offsettop = $this->height - self::FONT['price']['line_height'] - self::MARGIN['price']['bottom'];
        imagettftext(
            $this->card,
            self::FONT['price']['size'],
            0,
            self::MARGIN['price']['left'],
            $this->offsettop + self::FONT['price']['line_height'],
            $this->colors['price'],
            self::FONT['price']['uri'],
            $price
        );
    }
    private function draw_old_price($price, $old_price)
    {
        $box =imageftbbox(self::FONT['price']['size'], 0, self::FONT['price']['uri'], $price);
        $xr = abs(max($box[2], $box[4]));
        $xy = abs(max($box[5], $box[7]));
        $offset_left = self::MARGIN['price']['left'] + self::MARGIN['old_price']['left'] + $xr + 10;
        imagettftext(
            $this->card,
            self::FONT['old_price']['size'],
            0,
            $offset_left,
            $this->offsettop + $xy+2,
            $this->colors['old_price'],
            self::FONT['old_price']['uri'],
            $old_price
        );
        //draw stroke
        $box =imageftbbox(self::FONT['old_price']['size'], 0, self::FONT['old_price']['uri'], $old_price);
        $yr = abs(max($box[5], $box[7]));
        $y = $this->offsettop + self::FONT['old_price']['line_height']-round($yr/2)+14;
        $x1 = $offset_left-3;
        $x2 = $offset_left + abs(max($box[2], $box[4]))+6;

        imageline($this->card, $x1, $y, $x2, $y, $this->colors['old_price']);
    }
    private function draw_weight($weight)
    {
        $box =imageftbbox(self::FONT['price']['size'], 0, self::FONT['price']['uri'], '0');
        $xy = abs(max($box[5], $box[7]));
        $box =imageftbbox(self::FONT['weight']['size'], 0, self::FONT['weight']['uri'], $weight);
        $xr = abs(max($box[2], $box[4]));
        imagettftext(
            $this->card,
            self::FONT['weight']['size'],
            0,
            self::SIZE['width']-self::MARGIN['weight']['right']-$xr,
            $this->offsettop + $xy+2,
            $this->colors['weight'],
            self::FONT['weight']['uri'],
            $weight
        );
    }
    private function draw_index($index)
    {
        //draw bg
        $size = getimagesize(self::MATERIALS['index_background_url']);
        $im = imagecreatefrompng(self::MATERIALS['index_background_url']);
        $offset = [30, 30];
        imagecopy(
            $this->card,
            $im,
            $offset[0], $offset[1],
            0, 0,
            $size[0], $size[1]
        );
        //draw number
        $box =imageftbbox(self::FONT['index']['size'], 0, self::FONT['index']['uri'], $index);
        $xr = abs(max($box[2], $box[4]));
        $yr = abs(max($box[5], $box[7]));
        $x = intval(($size[0] - $xr) / 2);
        $y = intval(($size[0] + $yr) / 2);
        imagettftext(
            $this->card,
            self::FONT['index']['size'],
            0,
            $x+$offset[0],
            $y+$offset[1],
            $this->colors['index'],
            self::FONT['index']['uri'],
            $index
        );
    }
    private function draw_action_overlay()
    {
        $size = getimagesize(self::MATERIALS['action_overlay_url']);
        $im = imagecreatefrompng(self::MATERIALS['action_overlay_url']);
        $offset = [0, 0];
        imagecopy(
            $this->card,
            $im,
            $offset[0], $offset[1],
            0, 0,
            $size[0], $size[1]
        );
    }

    public function draw_card($params = [])
    {
        imagefill($this->card, 0, 0, $this->colors['background']);
        if (isset($params['thumb_url'])) {
            $url = $params['thumb_url'];
        } else {
            $url = self::DEFAULT_URI_THUMB_IMAGE;
        }
        if (!$this->get_thumb($url)) {
            $this->get_thumb(self::DEFAULT_URI_THUMB_IMAGE);
        }
        $this->thumb_overlay();

        if (isset($params['header'])) {
            self::draw_header($params['header']);
        }
        if (isset($params['description'])) {
            if ($params['description'] != '') {
                self::draw_description($params['description']);
            } else {
                self::draw_description('This product doesn\'t have description');
            }
        }
        if (isset($params['price'])) {
            self::draw_price($params['price']);
            if (isset($params['old_price'])){
                self::draw_old_price($params['price'], $params['old_price']);
                self::draw_action_overlay();
            }
        }
        if (isset($params['weight'])) {
            self::draw_weight($params['weight']);
        }
        if (isset($params['index'])) {
            self::draw_index($params['index']);
        }
        if ((!isset($params['price'])) || (!isset($params['old_price']))) {
            $this->draw_border();
        }
        return $this;
    }
    public function save($filename)
    {
        imagepng($this->card, $filename);
    }

    function __destruct()
    {
        imagedestroy($this->card);
    }
}