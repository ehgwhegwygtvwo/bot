<?php

namespace crm\mobidel;

/**
 * parse menu from for interaction with chat bot
 * Class Parse_menu
 * @package crm\mobidel
 */
class Parse_menu
{
    /**
     * create configuration map for products that may be configured
     * @param $product
     * @return array
     */
    private static function create_map($product)
    {
        $map = [
            'ids' => [
                'parameters' => [],
                'values' => []
            ],
            'names' => [
                'parameters' => [],
                'values' => []
            ]
        ];
        $map_selected = [];
        for ($i = 0; $i < count($product->childrens); $i++) {
            if (!isset($product->childrens[$i]->description->priceParameters)) {
                file_put_contents('log.txt', json_encode($product));
            }
            $priceParameters = $product->childrens[$i]->description->priceParameters;
            for ($j = 0; $j < count($priceParameters); $j++) {
                $parameterID = $priceParameters[$j]->priceParameterID;
                $parameterName = $priceParameters[$j]->priceParameterName;
                $parameterValueID = $priceParameters[$j]->priceParameterValueID;
                $parameterValueName = $priceParameters[$j]->priceParameterValueName;
                //new parameter
                $index_of_parameter = array_search($parameterID, $map['ids']['parameters']);
                if ($index_of_parameter === false) {
                    array_push($map['ids']['parameters'], $parameterID);
                    array_push($map['names']['parameters'], $parameterName);
                    $index_of_parameter = count($map['ids']['parameters'])-1;
                }

                if (isset($map['ids']['values'][$index_of_parameter])) {
                    if (array_search($parameterValueID, $map['ids']['values'][$index_of_parameter]) === false) {
                        array_push($map['ids']['values'][$index_of_parameter], $parameterValueID);
                        array_push($map['names']['values'][$index_of_parameter], $parameterValueName);
                    }
                } else {
                    $map['ids']['values'][$index_of_parameter] = [$parameterValueID];
                    $map['names']['values'][$index_of_parameter] = [$parameterValueName];
                    array_push($map_selected, $parameterValueID);
                }
            }
        }
        return $map;
    }

    /**
     * childrens have some types products: simple products,
     * configuration products and subcategory products.
     * If it then need give only childrens.
     *
     * @param $childrens
     * @param array $products
     * @return array
     */
    private static function parse_childrens($childrens, $products = [])
    {
        foreach ($childrens as $i => $product) {
            if (count($product->childrens) === 0) {//simple product
                array_push($products, $product);
            } else {
                //configuration product
                if ($product->childrens[0]->description->usePriceParameter == '1') {
                    $map = self::create_map($product);
                    $product->map = $map;
                    array_push($products, $product);
                } else {//subcategory product
                    $products = array_merge($products, $product->childrens);
                }
            }
        }
        return $products;
    }

    /**
     * parse menu categories
     * @param $menu
     * @return array
     */
    public static function parse($menu)
    {
        $categories = [];
        foreach ($menu as $i => $item) {
            $category = new \stdClass();
            $category->childrens = self::parse_childrens($menu[$i]->childrens);
            $category->description = $menu[$i]->description;
            array_push($categories, $category);
        }
        return $categories;
    }
}