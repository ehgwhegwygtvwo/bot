<?php

require_once "app.php";

$event = json_decode(file_get_contents('php://input'));

$_BOT_ID = -1; //this is super global variable for all application

if (!$event) {
    echo 'no event';
    exit(400);
}

try {
    switch ($event->type) {
//    switch ('f') {
        case СALLBACK_API_EVENT_CONFIRMATION:
            $group = \db\Group::get_group_by_id($event->group_id);
            \db\Group::set_confirmed($event->group_id);
            echo $group->server_confirm_code;
            break;
        case СALLBACK_API_EVENT_MESSAGE_NEW:
            $from_id = $event->object->from_id;
            $payloads = \db\Common::fetch_chat_payloads($event->group_id, $from_id);
            $payloads->vk_user_id = $from_id;
            $_BOT_ID = $payloads->bot_id;
            \vk\Vk_api::set_token($payloads->token);
            if ($output = \executor\Executor::execute($event, $payloads)) {
                $output->setToken($payloads->token);
                $output->send($from_id);
            }
            echo('ok');
            break;
        case СALLBACK_API_EVENT_MESSAGE_NEW_TEST:
            $from_id = $event->object->from_id;
            $payloads = \db\Common::fetch_chat_payloads($event->group_id, $from_id);
            $payloads->vk_user_id = $from_id;
            $_BOT_ID = $payloads->bot_id;
            \vk\Vk_api::set_token($payloads->token);
            try {
                $output = \executor\Executor::execute($event, $payloads);
                echo json_encode([
                    'output' => $output->getOutput(),
                    'keyboard' => json_decode($output->getKeyboard()),
                    'attachment' => $output->getAttachment(),
                    'state' => $payloads->state
                ]);
            } catch (\Exception $e) {
                throw new \Exception('Event СALLBACK_API_EVENT_MESSAGE_NEW_TEST error', 500, $e);
            }
            break;
        default:
            try {
                throw new Exception('another error');
            } catch (\Exception $e) {
                throw new Exception('Unsupported event', 1, $e);
            }
            break;
    }
} catch (\Exception $e) {
    $StackErrors = [];
    do {
        array_unshift($StackErrors, [
            'line' => $e->getLine(),
            'file' =>$e->getFile(),
            'message' => $e->getMessage(),
        ]);
    } while ($e = $e->getPrevious());
    echo json_encode([
        'error' => [
            'stackErrors' => $StackErrors
        ],
    ]);
}