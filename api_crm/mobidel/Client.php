<?php

namespace api_crm\mobidel;

class Client extends Shop
{

    public static function createClient($phone, $password, $username = '', $email = '')
    {
        if (self::$sessionID === '') {
            self::init();
        }
        $res = \Request::req([
            'url' => self::$api_url,
            'method' => 'POST',
            'params' => [
                'action' 	=> 'createClient',
                'phone' 	=> $phone,
                'password' 	=> $password,
                'username' 	=> $username,
                'email' 	=> $email
            ],
            'cookie' => self::$sessionID
        ]);
        return $res;
    }

    /**
     * authenticate client in mobidel
     * @param $sessionID
     * @param $login
     * @param $password
     * @return string
     */
    public static function authClient($login = '79634827999', $password = '123456')
    {
        if (self::$sessionID === '') {
            self::init();
        }
        $res = \Request::req([
            'url' => self::$api_url,
            'method' => 'POST',
            'params' => [
                'action' => 'authClient',
                'phone' => $login,
                'password' => $password
            ],
            'curlopt' => [
                [
                    'const' => CURLOPT_RETURNTRANSFER,
                    'value' => 1
                ]
            ],
            'cookie' => self::$sessionID
        ]);
        return $res;
    }

    public static function checkPIN($pin, $phone, $deviceID)
    {
        if (self::$sessionID === '') {
            self::init();
        }
        $res = \Request::req([
            'url' => self::$api_url,
            'method' => 'POST',
            'params' => [
                'action' => 'checkPIN',
                'pin' => $pin,
                'phone' => $phone,
                'deviceID' => $deviceID
            ],
            'cookie' => self::$sessionID
        ]);
        return $res;
    }

    public static function register($phone, $password, $userName='', $email='')
    {
        $res = self::createClient($phone, $password, $userName, $email);
        switch ($res['result']) {
            case self::CLIENT_ADDED:
                echo json_encode($res, JSON_UNESCAPED_UNICODE);
                break;
            case self::REGISTER_ERROR:
                throw new \Exception("register error");
                break;
        }
    }
}