<?php

namespace api_crm\mobidel;

/**
 * У api мобидел нет адекватной системы вывода исключений.
 * по этому я провожу инициализацию каждый раз, когда мне
 * нужна какая то функция завясящаю от переменных сессии.
 *
 * Class MobidelAPI
 * @package api_crm
 */
class Shop
{
    private static
        $appID = '1',
        $appKey = '123456',
        $cityID = '1',
        $restaurantID = '1621593598900183063';
    protected static
        $api_url = 'http://mobidel1.18r.ru:96/api/shop.php',
        $sessionID = '';

    /**
     * @param string $restaurantID
     */
    public static function setRestaurantID($restaurantID)
    {
        self::$restaurantID = $restaurantID;
    }
    /**
     * @param string $cityID
     */
    public static function setCityID($cityID)
    {
        self::$cityID = $cityID;
    }
    /**
     * @param string $appKey
     */
    public static function setAppKey($appKey)
    {
        self::$appKey = $appKey;
    }
    /**
     * @param string $appID
     */
    public static function setAppID($appID)
    {
        self::$appID = $appID;
    }

    /**
     * call getApplicationData, setClientCity and getRestaurantMenu, and save $sessionID
     */
    protected static function init()
    {
        if (self::$appID && self::$appKey && self::$cityID && self::$restaurantID) {
            self::$sessionID = self::getApplicationData(self::$appID, self::$appKey);
            self::setClientCity(self::$cityID);
            //this function require execute else checkOrderSumm won work
            self::getRestaurantMenu(self::$restaurantID);
        }
    }

    /**
     * @param $appID
     * @param $appKey
     * @return mixed
     */
    private static function getApplicationData($appID, $appKey)
    {
        $response = \Request::req([
            'method' => 'get',
            'url' => self::$api_url,
            'params' => [
                'action' => 'getApplicationData',
                'appID' => $appID,
                'appKey' => $appKey
            ],
            'curlopt' => [
                [
                    'const' => CURLOPT_RETURNTRANSFER,
                    'value' => 1
                ], [
                    'const' => CURLOPT_HEADER,
                    'value' => 1
                ]
            ]
        ]);
        $data = explode("\r\n\r\n", $response);
        $headers = $data[0];
        preg_match('/Set-Cookie: (PHPSESSID=\w+)/', $headers, $matches);
        $sessionID = $matches[1];
        return $sessionID;
    }

    /**
     * @param $cityID
     * @return bool|mixed|null
     */
    private static function setClientCity($cityID)
    {
        $res = \Request::req([
            'url' => self::$api_url,
            'method' => 'GET',
            'params' => [
                'action' => 'setClientCity',
                'cityID' => $cityID
            ],
            'curlopt' => [
                [
                    'const' => CURLOPT_RETURNTRANSFER,
                    'value' => 1
                ], [
                    'const' => CURLOPT_COOKIE,
                    'value' => self::$sessionID
                ]
            ]
        ]);
        return $res;
    }

    /**
     * get order prices by articles
     * @param $sessionID
     * @param $articles
     * @return mixed
     */
    public static function checkOrderSumm($articles, $bonusPay)
    {
        if (self::$sessionID === '') {
            self::init();
        }
        $res = \Request::req([
            'url' => self::$api_url . '?action=checkOrderSumm',
            'method' => 'POST',
            'curlopt' => [
                [
                    'const' => CURLOPT_RETURNTRANSFER,
                    'value' => 1
                ], [
                    'const' => CURLOPT_POSTFIELDS,
                    'value' => json_encode([
                        "articles" => $articles,
                        'persons' => '',
                        'bonusPay' => $bonusPay,
                        'paymentMethod' => '',
                        'street' => '',
                        'home' => '',
                        'building' => '',
                        'room' => '',
                        'promoCode' => '',
                        'independent' => '',
                        'warehouseID' => ''
                    ])
                ], [
                    'const' => CURLOPT_COOKIE,
                    'value' => self::$sessionID
                ]
            ]
        ]);
        return json_decode($res);
    }

    /**
     * get menu
     * @param string $restaurantID
     * @return bool|mixed|null
     */
    public static function getRestaurantMenu($restaurantID = '')
    {
        if (self::$sessionID === '') {
            self::init();
        }
        if ($restaurantID == '') {
            $restaurantID = self::$restaurantID;
        }
        $res = \Request::req([
            'url' => self::$api_url,
            'method' => 'GET',
            'params' => [
                'action' => 'getRestaurantMenu',
                'restaurantID' => $restaurantID
            ],
            'curlopt' => [
                [
                    'const' => CURLOPT_RETURNTRANSFER,
                    'value' => 1
                ], [
                    'const' => CURLOPT_COOKIE,
                    'value' => self::$sessionID
                ]
            ]
        ]);
        return $res;
    }

    /**
     * get warehouses
     * @param int $sessionID
     */
    public static function getWarehouses()
    {
        if (self::$sessionID === '') {
            self::init();
        }
        $res = \Request::req([
            'url' => self::$api_url,
            'method' => 'GET',
            'params' => [
                'action' => 'getWarehouses',
            ],
            'curlopt' => [
                [
                    'const' => CURLOPT_RETURNTRANSFER,
                    'value' => 1
                ], [
                    'const' => CURLOPT_COOKIE,
                    'value' => self::$sessionID
                ]
            ]
        ]);
        return json_decode($res);
    }
}













