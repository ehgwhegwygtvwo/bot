<?php

namespace api;

use TinyException\TinyException as TE;

include "../app.php";

/**
 *
 */
class Owner
{
    public static function create_owner($phone)
    {
        $owner = new Owner($phone);
        return Owner::save($owner);
    }
    public static function change_phone($phone, $new_phone)
    {
        $owner = \db\Owner::get_owner_by_phone($phone);
        if ($owner === false) {
            throw new TE(TE::ERR_API_UNKNOWN_OWNER);
        }
        return \db\Owner::change_phone($owner, $new_phone);
    }
    public static function get_tokens($phone)
    {
        $rows = \db\Common::fetch_all_tokens($phone);
//        mask tokens
        for ($i = 0; $i < count($rows); $i++) {
            $token = $rows[$i]->token;
            $begin = substr($token, 0, 4);
            $end = substr($token, -4,4);
            $rows[$i]->token = $begin.' ******** '.$end;
        }
        return $rows;
    }
}

try {
    if (isset($_POST['action'])) {
        switch ($_POST['action']) {
            case 'create_owner':
                if (!isset($_POST['phone'])) {
                    throw new TE(TE::ERR_API_PARAMETERS_FAIL);
                }
                if (Owner::create_owner($_POST['phone']) === false) {
                    throw new TE(TE::ERR_API_OWNER_ALREADY_EXISTS);
                }
                break;
            case 'change_phone':
                if (!isset($_POST['phone']) || !isset($_POST['new_phone'])) {
                    throw new TE(TE::ERR_API_PARAMETERS_FAIL);
                }
                Owner::change_phone($_POST['phone'], $_POST['new_phone']);
                break;
            case 'get_tokens':
                if (!isset($_POST['phone'])) {
                    throw new TE(TE::ERR_API_PARAMETERS_FAIL);
                }
                $tokens = Owner::get_tokens($_POST['phone']);
                echo json_encode($tokens);
                break;
            default:
                throw new TE(TE::ERR_API_PARAMETERS_FAIL);
                break;
        }
    }
    else {
        throw new TE(TE::ERR_API_NO_ACTION);
    }
} catch (\Exception $e) {
    header("HTTP/1.0 ".$e->getCode()." ".$e->getMessage(), TRUE, $e->getCode());
}