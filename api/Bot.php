<?php

namespace api;

use TinyException\TinyException as TE;

include "../app.php";

/**
 * Web api for deploying of bot
 *
 * Gets source - set of commands and save this by token
 */
class Bot{
    /**
     * Build bot by token
     *
     * @param string $token_id
     * @param string $source
     * @param integer $type
     * @throws \Exception
     */
    public static function build_bot($token_id, $source, $type)
    {
        $namespace = '\\translator\\';
        switch ($type) {
            case MOBIDEL_BOT_TYPE:
                $translator_name = 'TranslatorMobidel';
                break;
            default:
                throw new \Exception("unexpected bot type='$type'");
                break;
        }
        try {
            $program = call_user_func([$namespace.$translator_name, 'translate'], $source);
        } catch (\Exception $e) {
            throw new \Exception("error programm translate", 0, $e->getPrevious());
        }
        if ($bot_id = \db\Bot::update($token_id, $program, $source, $type)) {
            \db\State::drop_states($bot_id);
        }
    }

    public static function fetch_bots_data($owner_phone)
    {
        $owner = \db\Owner::get_owner_by_phone($owner_phone);
        if (!$owner) {
            throw new \Exception("owner undefined", 401);
        }
        $result = \db\Common::fetch_bots_data($owner_phone);
        return $result;
    }

    public static function bot_remove($bot_id)
    {
        \db\State::drop_states($bot_id);
        \db\Bot::bot_remove($bot_id);
    }

    public static function fetchBot($bot_id, $user_phone)
    {
        $bot = \db\Bot::fetch_bot($bot_id);
        $token_id = $bot->token_id;
        $token = \db\Token::get_token_by_id($token_id);
        $group_id = $token->group_id;
        $group = \db\Group::get_group_by_id($group_id);
        $owner_id = $group->owner_id;
        $user = \db\Owner::get_owner_by_phone($user_phone);

        if ($owner_id != $user->id) {
            throw new TE(TE::ERR_API_USER_IS_NOT_OWNER);
        }

        return $bot;
    }

    /**
     * check correct connection parameters
     * @param string $bot_type
     * @param array $params
     * @return array
     */
    public static function check_params($bot_type, $params)
    {
        switch ($bot_type){
            case 'mobidel':
                if (isset($params['appId'])  &&
                    isset($params['appKey']) &&
                    isset($params['cityId']) &&
                    isset($params['restaurantId']))
                {
                    \api_crm\mobidel\Shop::setAppID($params['appId']);
                    \api_crm\mobidel\Shop::setAppKey($params['appKey']);
                    \api_crm\mobidel\Shop::setCityID($params['cityId']);
                    \api_crm\mobidel\Shop::setRestaurantID($params['restaurantId']);
                } else {
                    throw new \Exception("parameters error", 400);
                }
                $res = \api_crm\mobidel\Shop::getRestaurantMenu();
                if ($res == 'null') {
                    $result = [0, 'bad parameters'];
                } else {
                    $result = [1, 'correct parameters'];
                }
                break;
            default:
                throw new \Exception("unknown type of bot '$bot_type'", 400);
                break;
        }
        return $result;
    }
}

try {
    if (isset($_POST['action'])) {
        switch ($_POST['action']) {
            case 'build':
                if (!isset($_POST['token_id']) || !isset($_POST['source']) || !isset($_POST['type'])) {
                    throw new TE(TE::ERR_API_PARAMETERS_FAIL);
                }
                Bot::build_bot($_POST['token_id'], $_POST['source'], $_POST['type']);
                break;
            case 'fetch_bots_data':
                if (!isset($_POST['owner_phone'])) {
                    throw new TE(TE::ERR_API_PARAMETERS_FAIL);
                }
                $result = Bot::fetch_bots_data($_POST['owner_phone']);
                echo json_encode($result);
                break;
            case 'bot_remove':
                if (!isset($_POST['bot_id'])) {
                    throw new TE(TE::ERR_API_PARAMETERS_FAIL);
                }
                Bot::bot_remove($_POST['bot_id']);
                break;
            case 'fetch_bot':
                if (!isset($_POST['bot_id']) || !isset($_POST['user_phone'])) {
                    throw new TE(TE::ERR_API_PARAMETERS_FAIL);
                }
                $result = Bot::fetchBot($_POST['bot_id'], $_POST['user_phone']);
                echo json_encode($result);
                break;
            case 'check_params':
                if (!isset($_POST['bot_type']) && !isset($_POST['params'])) {
                    throw new TE(TE::ERR_API_PARAMETERS_FAIL);
                }
                $result = Bot::check_params($_POST['bot_type'], $_POST['params']);
                echo json_encode($result);
                break;
            default:
                throw new TE(TE::ERR_API_UNEXPECTED_ACTION);
                break;
        }
    } else {
        throw new TE(TE::ERR_API_NO_ACTION);
    }
} catch (\Exception $e) {
    header("HTTP/1.0 ".$e->getCode()." ".$e->getMessage(), TRUE, $e->getCode());
}



