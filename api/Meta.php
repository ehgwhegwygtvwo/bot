<?php

namespace api;

use TinyException\TinyException as TE;

include "../app.php";

class Meta
{
    /**
     * @param string $bot_id
     * @param array $meta
     * @return false|int
     */
    public static function set_meta($bot_id, array $meta)
    {
        return \db\Meta::update_meta($bot_id, $meta);
    }

    /**
     * @param string $bot_id
     * @return array
     */
    public static function get_meta($bot_id)
    {
        $meta = \db\Meta::get_meta_by_bot_id($bot_id);
        return json_encode($meta);
    }
}

try {
    if (isset($_POST['action'])) {
        switch ($_POST['action']) {
            case 'set_meta':
                if (!isset($_POST['bot_id']) || !isset($_POST['meta'])) {
                    throw new TE(TE::ERR_API_PARAMETERS_FAIL);
                }
                Meta::set_meta($_POST['bot_id'], $_POST['meta']);
                break;
            case 'get_meta':
                if (!isset($_POST['bot_id']) || ($_POST['bot_id'] == '')) {
                    throw new TE(TE::ERR_API_PARAMETERS_FAIL);
                }
                $json_meta = Meta::get_meta($_POST['bot_id']);
                echo $json_meta;
                break;
        }
    } else {
        throw new TE(TE::ERR_API_NO_ACTION);
    }
} catch (\Exception $e) {
    header("HTTP/1.0 ".$e->getCode()." ".$e->getMessage(), TRUE, $e->getCode());
}
