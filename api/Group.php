<?php

namespace api;

use TinyException\TinyException as TE;

include "../app.php";

/**
 * Web api for attach server to vk group
 */
class Group {
    /**
     * Create new group
     * @param $group_id
     * @param $phone
     * @param $server_confirm_code
     * @return false|int
     * @throws \Exception
     */
    public static function registerGroup($group_id, $phone, $server_confirm_code)
    {
        $owner = \db\Owner::get_owner_by_phone($phone);
        if (!$owner) {
            //TODO: Create new owner
            throw new TE(TE::ERR_API_UNKNOWN_OWNER);
        }
        try {
            $result = \db\Group::group_add($group_id, $owner->id, $server_confirm_code);
        } catch (\Exception $e) {
            switch ($err_code = $e->getCode()) {
                case TE::ERR_DB_GROUP_ALREADY_EXISTS:
                    \db\Group::set_confirmed_code($group_id, $server_confirm_code);
                    $result = \db\Group::get_group_by_id($group_id);
                    break;
                default:
                    throw new TE(TE::ERR_UNKNOWN_ERROR);
                    break;
            }
        }
        return $result;
    }

    public static function getConfirmed($group_id)
    {
        $group = \db\Group::get_group_by_id($group_id);
        return $group->confirmed;
    }
}

echo '1';

try {
    if (isset($_POST['action'])) {
        switch ($_POST['action']) {
            case 'register_group':
                if (!isset($_POST['group_id']) || !isset($_POST['phone']) || !isset($_POST['server_confirm_code'])) {
                    throw new TE(TE::ERR_API_PARAMETERS_FAIL);
                }
                \api\Group::registerGroup($_POST['group_id'], $_POST['phone'], $_POST['server_confirm_code']);
                break;
            case 'get_confirmed'://get status confirmed
                if (!isset($_POST['group_id'])) {
                    throw new TE(TE::ERR_API_PARAMETERS_FAIL);
                }
                echo \api\Group::getConfirmed(($_POST['group_id']));
                break;
            default:
                throw new TE(TE::ERR_API_UNEXPECTED_ACTION);
                break;
        }
    } else {
        throw new TE(TE::ERR_API_NO_ACTION);
    }
} catch (\Exception $e) {
    header("HTTP/1.0 ".$e->getCode()." ".$e->getMessage(), TRUE, $e->getCode());
}