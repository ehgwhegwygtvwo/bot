<?php

namespace api;

use TinyException\TinyException as TE;

include "../app.php";

class State
{
    public static function dropUserState($group_id, $vk_user_id)
    {
        $token_id = \db\Token::get_token_by_group_id($group_id)->id;
        $bot_id = \db\Bot::get_bot_by_token_id($token_id)->id;
        \db\State::dropState($bot_id, $vk_user_id);
        \db\Cart::dropUserState($bot_id, $vk_user_id);
    }

    public static function dropUserStates($vk_user_id)
    {
        \db\State::dropUserStates($vk_user_id);
    }
}

try {
    if (isset($_POST['action'])) {
        switch ($_POST['action']) {
            case 'drop_state':
                if (!isset($_POST['group_id']) || !isset($_POST['vk_user_id'])) {
                    throw new TE(TE::ERR_API_PARAMETERS_FAIL);
                }
                State::dropUserState($_POST['group_id'], $_POST['vk_user_id']);
                break;
            case 'drop_user_states':
                if (!isset($_POST['vk_user_id'])) {
                    throw new TE(TE::ERR_API_PARAMETERS_FAIL);
                }
                State::dropUserStates($_POST['vk_user_id']);
                break;
            default:
                throw new TE(TE::ERR_API_UNEXPECTED_ACTION);
                break;
        }
    } else {
        throw new TE(TE::ERR_API_NO_ACTION);
    }
} catch (\Exception $e) {
    header("HTTP/1.0 ".$e->getCode()." ".$e->getMessage(), TRUE, $e->getCode());
}