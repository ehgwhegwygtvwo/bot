<?php

namespace api;

use TinyException\TinyException as TE;

include "../app.php";

class Token{
    public static function add_token($token, $group_id)
    {
        $obj = new \db\Token($token, $group_id);
        return \db\Token::save($obj);
    }
}

try {
    if (isset($_POST['action'])) {
        switch ($_POST['action']) {
            case 'add_token':
                if (!isset($_POST['token']) || !isset($_POST['group_id'])) {
                    throw new TE(TE::ERR_API_PARAMETERS_FAIL);
                }
                $result = Token::add_token($_POST['token'], $_POST['group_id']);
                echo $result;
                break;
            default:
                throw new TE(TE::ERR_API_UNEXPECTED_ACTION);
                break;
        }
    } else {
        throw new TE(TE::ERR_API_NO_ACTION);
    }
} catch (\Exception $e) {
    header("HTTP/1.0 ".$e->getCode()." ".$e->getMessage(), TRUE, $e->getCode());
}