<?php

namespace api;

include "../app.php";

/**
 * Web api for update bots prototypes
 */
class Functions{
    private static
        $log;

    /**
     * Check admin key (no implements)
     * @param string $key
     * @return bool
     */
    private static function check_access($key)
    {
        return true;
    }

    /**
     * @param string $type type of bot example mobidel etc
     * @param string $key especially admin key (no implements)
     */
    public static function update($type, $key){
        if (self::check_access($key)) {
            switch ($type) {
                case 'mobidel_bot':
                    Mobidel_bot::define_prototype();
                    break;
                default:
                    Functions::log('error unexcepted bot type \'%s\'', $type);
                    break;
            }
            Functions::log('success');
        } else {
            Functions::log('error key not access');
        }
    }
    public static function log(){
        if (!self::$log) {
            self::$log = new \Logger('functions_manager');
        }
        $params=func_get_args();
        call_user_func_array(array(self::$log, 'log'), $params);
    }
}

if (isset($_GET['action'])) {
    switch ($_GET['action']) {
        case 'update_prototype':
            Functions::log('request update prototype \'%s\'', $_GET['type']);
            if (isset($_GET['type'])) {
                if (isset($_GET['key'])) {//super key
                    Functions::update($_GET['type'], $_GET['key']);
                } else {
                    Functions::log('error key undefined');
                }
            } else {
                Functions::log('error type undefined');
            }
            break;
        default:
            Functions::log('unexpected action \'%s\'', $_GET['action']);
            break;
    }
} else {
    Functions::log('no action');
    die();
}
//http://localhost/php/api/functions_manager.php?action=update_prototype&type=mobidel_bot&key=1