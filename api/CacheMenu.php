<?php

namespace api;

use TinyException\TinyException as TE;

include "../app.php";

class CacheMenu
{
    public static function caching_menu($bot_id)
    {
        $menu_id = \db\CacheMenu::caching_menu($bot_id);
        $row = \db\CacheMenu::get_cache_menu_by_id($menu_id);
        $menu = json_decode($row->menu);
        $number_of_categories = count($menu);
        \db\Meta::update_meta($bot_id, [
            [
                'key' => 'number_of_categories',
                'value' => $number_of_categories
            ]
        ]);
    }
}

try {
    if (isset($_POST['action'])) {
        switch ($_POST['action']) {
            case 'caching_menu':
                if (!isset($_POST['bot_id'])) {
                    throw new TE(TE::ERR_API_PARAMETERS_FAIL);
                }
                CacheMenu::caching_menu($_POST['bot_id']);
                break;
        }
    } else {
        throw new TE(TE::ERR_API_NO_ACTION);
    }
} catch (\Exception $e) {
    header("HTTP/1.0 ".$e->getCode()." ".$e->getMessage(), TRUE, $e->getCode());
}
