<?php

namespace vk;

abstract class Vk_api extends \Request
{
    const
        VK_API_VERSION = '5.80',
        VK_API_ENDPOINT = 'https://api.vk.com/method/';

    protected static $token;

    protected static function request($options)
    {
        return \Request::req($options);
    }

    /**
     * @param mixed $token
     */
    public static function set_token($token)
    {
        self::$token = $token;
    }
}