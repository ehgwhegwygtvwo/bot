<?php

namespace vk;

/**
 * Created by PhpStorm.
 * User: Вятка IT-3
 * Date: 04.09.2018
 * Time: 13:33
 */
class Messages extends Vk_api
{
    private static $api = 'messages';

    /**
     * Send message to vk user
     *
     * @param string $message content of message
     */
    public static function send_message($user_id, $message, $attachment, $keyboard, $access_token)
    {
        $params = [
            'v' => self::VK_API_VERSION,
            'user_id' => $user_id,
            'message' => $message,
            'attachment' => $attachment,
            'access_token' => $access_token,
            'random_id' => mt_rand(20, 99999999)
        ];
        if ($keyboard == '') {
            $params['keyboard'] = '{"buttons":[],"one_time":true}';
        } else {
            $params['keyboard'] = $keyboard;
        }
        $result = self::request([
            'url' => self::VK_API_ENDPOINT.self::$api.'.send',
            'method' => 'POST',
            'params' => $params,
        ]);
        return $result;
    }
}