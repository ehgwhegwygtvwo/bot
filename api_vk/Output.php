<?php

namespace vk;

class Output{

    private
        $output = '',
        $keyboard = '',
        $attachment = '',
        $token = '';

    /**
     * @return string
     */
    public function getOutput()
    {
        return $this->output;
    }

    /**
     * @return string
     */
    public function getKeyboard()
    {
        return $this->keyboard;
    }

    /**
     * @return string
     */
    public function getAttachment()
    {
        return $this->attachment;
    }

    /**
     * Vk constructor.
     */
    public function __construct($token = '')
    {
        $this->token = $token;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * Add text massage in output
     * @param string $text some text
     */
    public function message($text)
    {
        if ($this->output != '') {
            $this->output .= "\n\r";
        }
        $this->output .= $text;
    }

    /**
     * Set keyboard to massage in output
     * @param string $text some text
     */
    public function keyboard($keyboard)
    {
        if (is_array($keyboard) || is_object($keyboard)) {
            $keyboard = json_encode($keyboard, JSON_UNESCAPED_UNICODE);
        }
        $this->keyboard = $keyboard;
    }

    /**
     * Add media in output
     * @param string $photo link
     */
    public function attachment($attachment)
    {
        if ($this->attachment) {
            $this->attachment .= ",".$attachment;
        } else {
            $this->attachment = $attachment;
        }
    }

    /**
     * send output to user
     * @param $to
     * @throws \Exception
     */
    public function send($to)
    {
        if (($this->output == '') && ($this->attachment == '') && ($this->keyboard == '')) {
            throw new \Exception('no data for send');
        }
        Messages::send_message($to, $this->output, $this->attachment, $this->keyboard, $this->token);
    }

}
