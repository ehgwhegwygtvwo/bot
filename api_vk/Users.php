<?php

namespace vk;

class Users extends Vk_api
{
    /**
     * Send http request
     *
     * Sends an http-request based on the curl framework
     */
    protected static function request($options)
    {
        if (!isset($options['api'])) return false;
        //make url
        $ch = curl_init();
        $http_params = http_build_query($options['params']);
        $url = self::VK_API_ENDPOINT
            .$options['api']
            .'?'
            .$http_params;
        //set options
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if (isset($options['curlopt'])) {
            for ($i=0; $i < count($options['curlopt']); $i++) {
                $option = $options['curlopt'][$i];
                curl_setopt($ch, $option['const'], $option['value']);
            }
        }
        $response = curl_exec($ch);
        curl_close($ch);
        return json_decode($response);
    }
    /**
     * Get metadata about of user by its id
     *
     * @param string $user_id user vk id
     */
    public static function get_user_data($user_id)
    {
        return self::request([
            'api' => 'users.get',
            'params' => [
                'user_ids' => $user_id,
                'access_token' => self::$token,
                'v' => self::VK_API_VERSION,
            ],
            'curlopt' => [
                [
                    'const' => CURLOPT_RETURNTRANSFER,
                    'value' => 1
                ]
            ]
        ]);
    }
}