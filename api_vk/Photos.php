<?php

namespace vk;

class Photos extends Vk_api
{
    public static
        $api = 'photos';

    private static function get_messages_upload_server()
    {
        return self::request([
            'method' => 'get',
            'url' => self::VK_API_ENDPOINT.self::$api.'.getMessagesUploadServer',
            'params' => [
                'access_token' => self::$token,
                'v' => self::VK_API_VERSION,
            ],
            'curlopt' => [
                [
                    'const' => CURLOPT_RETURNTRANSFER,
                    'value' => 1
                ]
            ]
        ]);
    }

    private static function get_upload_url()
    {
        $data = json_decode(self::get_messages_upload_server());
        return $data->response->upload_url;
    }

    private static function upload_image($filename)
    {
        $url = self::get_upload_url();
        $file = new \CurlFile($filename);
        $request = [
            'method' => 'POST',
            'url' => $url,
            'params' => [
                'access_token' => self::$token,
                'photo' => $file,
                'v' => self::VK_API_VERSION,
            ],
            'curlopt' => [
                [
                    'const' => CURLOPT_RETURNTRANSFER,
                    'value' => 1
                ]
            ]
        ];
        $json = self::request($request);
        $data = json_decode($json);
        return $data;
    }

    public static function save_image($filename)
    {
        $data = self::upload_image($filename);
        return self::request([
            'method' => 'get',
            'url' => "https://api.vk.com/method/photos.saveMessagesPhoto",
            'params' => [
                'access_token' => self::$token,
                'photo' => $data->photo,
                'hash' => $data->hash,
                'server' => $data->server,
                'v' => self::VK_API_VERSION,
            ],
            'curlopt' => [
                [
                    'const' => CURLOPT_RETURNTRANSFER,
                    'value' => 1
                ]
            ]
        ]);
    }
}