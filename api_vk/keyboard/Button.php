<?php
/**
 * Created by PhpStorm.
 * User: Air
 * Date: 19.09.2018
 * Time: 22:41
 */

namespace vk\keyboard;

class Button
{
    public
        $action = null,
        $color = '';
    function __construct($label, $payload, $color = 'default')
    {
        $this->action = new Action($payload, $label);
        $this->color = $color;
    }
}