<?php

namespace vk\keyboard\patterns;

use vk\keyboard as Kb;

class MobidelPattern
{
    public static function categories_kb($categories_labels, $output_order_button, $one_time = false)
    {
        $max_in_row = 4;
        $buttons = [];
        $row = [];
        for ($i = 0; $i < count($categories_labels); $i++) {
            $button = new kb\Button($categories_labels[$i], json_encode(['value' => $i]), 'primary');
            array_push($row, $button);
            if (count($row) == $max_in_row) {
                array_push($buttons, $row);
                $row = [];
            }
        }
        // append empty buttons for same size buttons in menu
        if (count($row) > 0) {
            while (count($row) < $max_in_row)
            {
                $button = new kb\Button(' ', json_encode(['value' => 'no action']), 'default');
                array_push($row, $button);
            }
            array_push($buttons, $row);
        }
        $row = [
            new kb\Button('Выход', json_encode(['value' => -1]), 'primary'),
            new kb\Button('Корзина', json_encode(['value' => -2]),'positive'),
        ];
        if ($output_order_button === true) {
            array_push($row, new kb\Button('Оформить заказ', json_encode(['value' => -3]),'negative'));
        }
        array_push($buttons, $row);
        $kb = new kb\KeyBoard($one_time, $buttons);
        return $kb;
    }
    public static function products_kb($current_page, $per_page, $number_products, $long_step, $one_time = false)
    {
        $max_in_row = 4;
        $buttons = [];
        $row = [];
        //number of products
        for ($i = 0; $i < $per_page; $i++) {
            $index = ($current_page-1)*$per_page+$i;
            if ($index < $number_products) {
                $button = new kb\Button($index+1, json_encode(['value' => $index]), 'primary');
                array_push($row, $button);
                if (count($row) == $max_in_row) {
                    array_push($buttons, $row);
                    $row = [];
                }
            }
        }
        // append empty buttons for same size buttons in menu
        while (count($row) < $max_in_row) {
            $button = new kb\Button(' ', json_encode(['value' => 'no action']), 'default');
            array_push($row, $button);
        }
        array_push($buttons, $row);
        //navigate
        $row = [];

        if ($current_page - $long_step > 0) {
            array_push($row, new kb\Button('<<', json_encode(['value' => -1]), 'primary'));
        } else{
            array_push($row, new kb\Button(' ', json_encode(['value' => 'no action']), 'default'));
        }
        if ($current_page - 1 > 0) {
            array_push($row, new kb\Button('<', json_encode(['value' => -2]), 'primary'));
        } else{
            array_push($row, new kb\Button(' ', json_encode(['value' => 'no action']), 'default'));
        }

        if ($current_page + 1 <= ceil($number_products/$per_page)) {
            array_push($row, new kb\Button('>', json_encode(['value' => -3]), 'primary'));
        } else{
            array_push($row, new kb\Button(' ', json_encode(['value' => 'no action']), 'default'));
        }
        if ($current_page + $long_step <= ceil($number_products/$per_page)) {
            array_push($row, new kb\Button('>>', json_encode(['value' => -4]), 'primary'));
        } else{
            array_push($row, new kb\Button(' ', json_encode(['value' => 'no action']), 'default'));
        }
        array_push($buttons, $row);

        //other
        array_push($buttons, [
            new kb\Button('Назад', json_encode(['value' => -7]), 'primary'),
            new kb\Button('Корзина', json_encode(['value' => -5]),'positive'),
            new kb\Button('Сделать заказ', json_encode(['value' => -6]), 'negative'),
        ]);
        $kb = new kb\KeyBoard($one_time, $buttons);
        return $kb;
    }
    public static function simple_product_control_kb($quantity, $have_toppings = false, $one_time = false)
    {
        $buttons = [[]];
        if ($quantity < 1) {
            array_push($buttons[0], new kb\Button('-', json_encode(['value' => 'no action']), 'default'));
        } else {
            array_push($buttons[0], new kb\Button('-', json_encode(['value' => -1]), 'primary'));
        }
        array_push($buttons[0], new kb\Button($quantity, json_encode(['value' => 'no action']), 'default'));
        array_push($buttons[0], new kb\Button('+', json_encode(['value' => -2]), 'primary'));
        if (($have_toppings === true) && ($quantity > 0)) {
            array_push($buttons, [
                new kb\Button('Сбросить', json_encode(['value' => -3]), 'primary'),
                new kb\Button('Топпинги', json_encode(['value' => -4]), 'primary')
            ]);
        }
        array_push($buttons, [
            new kb\Button('Меню', json_encode(['value' => -5]), 'primary'),
            new kb\Button('Корзина', json_encode(['value' => -6]), 'positive'),
            new kb\Button('Оформить заказ', json_encode(['value' => -7]), 'negative'),
        ]);
        return new kb\KeyBoard($one_time, $buttons);
    }

    public static function configure_product_control_kb($quantity = 0, $one_time = false)
    {
        $buttons = [];
        array_push($buttons,[
            new kb\Button('Конфигурация', json_encode(['value' => -8]), 'positive'),
            new kb\Button('Корзина', json_encode(['value' => -6]), 'primary')
        ],[
            new kb\Button('Назад в меню', json_encode(['value' => -5]), 'primary'),
            new kb\Button('Оформить заказ', json_encode(['value' => -7]), 'negative')
        ]);

        return new kb\KeyBoard($one_time, $buttons);
    }

    /**
     * keyboard with list of product toppings
     * @param $semiproducts
     * @param bool $one_time
     * @return Kb\KeyBoard
     */
    public static function semiproducts_kb($semiproducts, $one_time = false)
    {
        $max_in_row = 2;
        $buttons = [];
        $row = [];
        for ($i = 0; $i < count($semiproducts); $i++) {
            $button = new kb\Button($semiproducts[$i]->name, json_encode(['value' => $i]), 'primary');
            array_push($row, $button);
            if (count($row) == $max_in_row) {
                array_push($buttons, $row);
                $row = [];
            }
        }
        // append empty buttons for same size buttons in menu
        if (count($row) > 0) {
            while (count($row) < $max_in_row)
            {
                $button = new kb\Button(' ', "[]", 'default');
                array_push($row, $button);
            }
            array_push($buttons, $row);
        }
        array_push($buttons, [
            new kb\Button('Назад', json_encode(['value' => -1]), 'primary'),
            new kb\Button('Корзина',  json_encode(['value' => -2]),'positive'),
            new kb\Button('Оформить заказ',  json_encode(['value' => -3]),'negative'),
        ]);
        $kb = new kb\KeyBoard($one_time, $buttons);
        return $kb;
    }

    /**
     * keyboard with control panel topping
     * @param $quantity
     * @param bool $one_time
     */
    public static function topping_control_kb($quantity, $one_time = false)
    {
        $buttons = [[]];
        if ($quantity < 1) {
            array_push($buttons[0], new kb\Button('-', json_encode(['value' => 'no action']), 'default'));
        } else {
            array_push($buttons[0], new kb\Button('-', json_encode(['value' => -1]), 'primary'));
        }
        array_push($buttons[0], new kb\Button($quantity, json_encode(['value' => 'no action']), 'default'));
        array_push($buttons[0], new kb\Button('+', json_encode(['value' => -2]), 'primary'));
        if ($quantity > 0) {
            array_push($buttons[0], new kb\Button('Сбросить', json_encode(['value' => -3]), 'primary'));
        }
        array_push($buttons, [
            new kb\Button('Назад', json_encode(['value' => -4]), 'primary'),
            new kb\Button('Корзина', json_encode(['value' => -5]), 'positive'),
            new kb\Button('Оформить заказ', json_encode(['value' => -6]), 'negative'),
        ]);
        return new kb\KeyBoard($one_time, $buttons);
    }

    /**
     * keyboard with control panel cart
     * @param $quantity
     * @param bool $one_time
     * @return kb\KeyBoard
     */
    public static function cart_kb($quantity, $one_time = false)
    {
        $max_in_row = 4;
        $buttons = [];
        $row = [];
        if ($quantity > 0) {
            for ($i = 0; $i < $quantity; $i++) {
                $button = new kb\Button($i+1, json_encode(['value' => $i]), 'primary');
                array_push($row, $button);
                if (count($row) == $max_in_row) {
                    array_push($buttons, $row);
                    $row = [];
                }
            }
            while (count($row) < $max_in_row) {
                $button = new kb\Button(' ', json_encode(['value' => 'no action']), 'default');
                array_push($row, $button);
            }
            array_push($buttons, $row);
            $row = [];
            array_push($row, new kb\Button('<<', json_encode(['value' => 'no action']), 'default'));
            array_push($row, new kb\Button('<', json_encode(['value' => 'no action']), 'default'));
            array_push($row, new kb\Button('>', json_encode(['value' => 'no action']), 'default'));
            array_push($row, new kb\Button('>>', json_encode(['value' => 'no action']), 'default'));
            array_push($buttons, $row);
        }
        array_push($buttons, [
            new kb\Button('Меню', json_encode(['value' => -1]), 'primary'),
            new kb\Button('Оформить заказ', json_encode(['value' => -2]), 'negative')
        ]);
        $kb = new kb\KeyBoard($one_time, $buttons);
        return $kb;
    }

    /**
     * select value of parameter
     * @param $id_parameter
     * @param $map
     * @param bool $one_time
     * @return Kb\KeyBoard
     */
    public static function price_parameter_kb($id_parameter, $map, $one_time = false)
    {
        $id_keys = array_keys($map['id']);
        $index_parameter = array_search($id_parameter, $id_keys);
        $name_keys =  array_keys($map['name']);
        $name = $name_keys[$index_parameter];
        $values = $map['name'][$name];
        $max_in_row = 4;
        $buttons = [];
        $row = [];
        for ($i = 0; $i < count($values); $i++) {
            $button = new kb\Button($values[$i], json_encode(['value' => $i]), 'primary');
            array_push($row, $button);
            if (count($row) == $max_in_row) {
                array_push($buttons, $row);
                $row = [];
            }
        }
        // append empty buttons for same size buttons in menu
        if (count($row) > 0) {
            while (count($row) < $max_in_row)
            {
                $button = new kb\Button(' ', json_encode(['value' => 'no action']), 'default');
                array_push($row, $button);
            }
            array_push($buttons, $row);
        }
        array_push($buttons, [
            new kb\Button('Отмена', json_encode(['value' => -1]), 'negative'),
            new kb\Button('Предыдущи параметр', json_encode(['value' => -2]),'primary'),
        ]);
        $kb = new kb\KeyBoard($one_time, $buttons);
        return $kb;
    }

    /**
     * when configuration was complete
     * @param bool $one_time
     * @return Kb\KeyBoard
     */
    public static function end_of_configuration_kb($one_time = false)
    {
        $buttons = [];
        array_push($buttons, [
            new kb\Button('Пересобрать', json_encode(['value' => 'no action']), 'default'),
            new kb\Button('Продолжить', json_encode(['value' => -3]),'primary'),
        ]);
        $kb = new kb\KeyBoard($one_time, $buttons);
        return $kb;
    }

    /**
     * settings order metadata and send order
     * @param $fields
     * @param $index_of_make_order_page
     * @param bool $one_time
     * @return Kb\KeyBoard
     */
    public static function order_kb($pm, $obtain_method, $bonusPay, $one_time = false)
    {
        $buttons = [];
        $row = [];
        if ($pm) {
            array_push($row, new kb\Button('Оплата: '.$pm, json_encode(['value' => -1]), 'primary'));
        } else {
            array_push($row, new kb\Button('Оплата', json_encode(['value' => -1]), 'positive'));
        }
        array_push($row, new kb\Button('Данные', json_encode(['value' => -2]), 'positive'));
        array_push($buttons, $row);
        //bonuses & obtain
        $row = [];
        if ($bonusPay === 1) {
            array_push($row, new kb\Button('Оплата бонусами: Да', json_encode(['value' => -3]), 'primary'));
        } else {
            array_push($row, new kb\Button('Оплата бонусами: Нет', json_encode(['value' => -3]), 'primary'));
        }
        switch ($obtain_method) {
            case -1:
                array_push($row, new kb\Button('Получение', json_encode(['value' => -4]), 'positive'));
                break;
            case 0:
                array_push($row, new kb\Button('Получение: доставка', json_encode(['value' => -4]), 'primary'));
                break;
            case 1:
                array_push($row, new kb\Button('Получение: самовывоз', json_encode(['value' => -4]), 'primary'));
                break;
        }
        array_push($buttons, $row);
        array_push($buttons, [
            new kb\Button('Меню', json_encode(['value' => -5]), 'primary'),
            new kb\Button('Предпросмотр', json_encode(['value' => -6]),'negative')
        ]);

        $kb = new kb\KeyBoard($one_time, $buttons);
        return $kb;
    }

    /**
     * @param bool $one_time
     * @return Kb\KeyBoard
     */
    public static function order_pm_kb($pm_index, $pm_map, $one_time = false)
    {
        $max_in_row = 3;
        $buttons = [];
        $row = [];
        for ($i = 0; $i < count($pm_map); $i++) {
            if ($i === $pm_index) {
                $button = new kb\Button($pm_map[$i], json_encode(['value' => $i]), 'positive');
            } else {
                $button = new kb\Button($pm_map[$i], json_encode(['value' => $i]), 'primary');
            }
            array_push($row, $button);
            if (count($row) == $max_in_row) {
                array_push($buttons, $row);
                $row = [];
            }
        }
        // append empty buttons for same size buttons in menu
        if (count($row) > 0) {
            while (count($row) < $max_in_row)
            {
                $button = new kb\Button(' ', json_encode(['value' => 'no action']), 'default');
                array_push($row, $button);
            }
            array_push($buttons, $row);
        }
        array_push($buttons, [
            new kb\Button('Назад', json_encode(['value' => -1]), 'primary'),
            new kb\Button('Принять', json_encode(['value' => -2]),'positive'),
        ]);
        $kb = new kb\KeyBoard($one_time, $buttons);
        return $kb;
    }
    /**
     * @param bool $one_time
     * @return Kb\KeyBoard
     */
    public static function order_init_kb($fields, $page_index, $max_index, $one_time = false)
    {
        $buttons = [];
        for ($i = 0; $i < count($fields); $i++) {
            $row = [];
            if ($fields[$i]['required'] === true) {
                $label = '*'.$fields[$i]['label'];
                array_push($row, new kb\Button($label, json_encode(['value' => $i]), 'primary'));
                if ($fields[$i]['value'] === '') {
                    array_push($row, new kb\Button('Указать', json_encode(['value' => $i]), 'positive'));
                } else {
                    array_push($row, new kb\Button('Редактировать', json_encode(['value' => $i]), 'primary'));
                }
            } else {
                array_push($row, new kb\Button($fields[$i]['label'], json_encode(['value' => $i]), 'primary'));
                if ($fields[$i]['value'] === '') {
                    array_push($row, new kb\Button('Указать', json_encode(['value' => $i]), 'primary'));
                } else {
                    array_push($row, new kb\Button('Редактировать', json_encode(['value' => $i]), 'primary'));
                }
            }
            array_push($buttons, $row);
        }
        $row = [];
        if ($page_index === 1) {
            array_push($row, new kb\Button('', json_encode(['value' => 'no action']), 'default'));
        } else {
            array_push($row, new kb\Button('<', json_encode(['value' => -1]), 'primary'));
        }
        array_push($row, new kb\Button($page_index, json_encode(['value' => 'no action']), 'default'));
        if ($page_index === $max_index) {
            array_push($row, new kb\Button('', json_encode(['value' => 'no action']), 'default'));
        } else {
            array_push($row, new kb\Button('>', json_encode(['value' => -2]), 'primary'));
        }

        array_push($buttons, $row);
        array_push($buttons, [
            new kb\Button('Назад', json_encode(['value' => -3]), 'primary'),
            new kb\Button('Готово', json_encode(['value' => -4]), 'primary')
        ]);
        $kb = new kb\KeyBoard($one_time, $buttons);
        return $kb;
    }
    /**
     * order obtain ways
     * @param bool $one_time
     * @return Kb\KeyBoard
     */
    public static function order_ow_kb($obtain_method = -1, $one_time = false)
    {
        $buttons = [];
        switch ($obtain_method) {
            case -1:
                array_push($buttons, [
                    new kb\Button('Самовывоз', json_encode(['value' => -1]), 'primary'),
                    new kb\Button('Доставка', json_encode(['value' => -2]),'primary'),
                ]);
                break;
            case 0:
                array_push($buttons, [
                    new kb\Button('Самовывоз', json_encode(['value' => -1]), 'primary'),
                    new kb\Button('Доставка', json_encode(['value' => -2]),'positive'),
                ]);
                break;
            case 1:
                array_push($buttons, [
                    new kb\Button('Самовывоз', json_encode(['value' => -1]), 'positive'),
                    new kb\Button('Доставка', json_encode(['value' => -2]),'primary'),
                ]);
                break;
        }
        array_push($buttons, [
            new kb\Button('Назад', json_encode(['value' => -3]), 'primary'),
            new kb\Button('Принять', json_encode(['value' => -4]),'positive'),
        ]);

        $kb = new kb\KeyBoard($one_time, $buttons);
        return $kb;
    }

    public static function warehouses($warehouses, $one_time = false)
    {
        $max_in_row = 4;
        $buttons = [];
        $row = [];
        for ($i = 0; $i < count($warehouses); $i++) {
            $button = new kb\Button($i+1, json_encode(['value' => $i]), 'primary');
            array_push($row, $button);
            if (count($row) == $max_in_row) {
                array_push($buttons, $row);
                $row = [];
            }
        }
        // append empty buttons for same size buttons in menu
        if (count($row) > 0) {
            while (count($row) < $max_in_row)
            {
                $button = new kb\Button(' ', json_encode(['value' => 'no action']), 'default');
                array_push($row, $button);
            }
            array_push($buttons, $row);
        }
        $row = [
            new kb\Button('<', json_encode(['value' => 'no action']), 'default'),
            new kb\Button('1', json_encode(['value' => 'no action']), 'default'),
            new kb\Button('>', json_encode(['value' => 'no action']),'default'),
        ];
        array_push($buttons, $row);
        $row = [
            new kb\Button('Назад', json_encode(['value' => -3]), 'primary'),
            new kb\Button('Принять', json_encode(['value' => -4]),'positive'),
        ];
        array_push($buttons, $row);
        $kb = new kb\KeyBoard($one_time, $buttons);
        return $kb;
    }
    public static function preview_order($correct, $one_time = false)
    {
        $buttons = [
            [
                new kb\Button('Назад', json_encode(['value' => -1]), 'primary')
            ]
        ];
        if ($correct) {
            array_push($buttons[0], new kb\Button('Отправить', json_encode(['value' => -2]), 'primary'));
        } else {
            array_push($buttons[0], new kb\Button('Отправить', json_encode(['value' => 'no action']), 'default'));
        }
        $kb = new kb\KeyBoard($one_time, $buttons);
        return $kb;
    }

    public static function register_kb($one_time = false)
    {
        $buttons = [
            [
                new kb\Button('Назад', json_encode(['value' => -5]), 'primary'),
                new kb\Button('Указать номер', json_encode(['value' => -7]), 'primary')
            ]
        ];
        $kb = new kb\KeyBoard($one_time, $buttons);
        return $kb;
    }
    public static function sms($one_time = false)
    {
        $buttons = [
            [
                new kb\Button('Отмена', json_encode(['value' => -3]), 'primary'),
                new kb\Button('Ввести код', json_encode(['value' => -2]), 'primary')
            ]
        ];
        $kb = new kb\KeyBoard($one_time, $buttons);
        return $kb;
    }

    public static function emptyKeyboard($one_time = true)
    {
        $buttons = [[]];
        $kb = new kb\KeyBoard($one_time, $buttons);
        return $kb;
    }
}