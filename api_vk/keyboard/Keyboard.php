<?php

namespace vk\keyboard;

class Keyboard
{
    public
        $one_time = false,
        $buttons = null;
    function __construct($one_time = false, $buttons)
    {
        $this->one_time = $one_time;
        $this->buttons = $buttons;
    }
}