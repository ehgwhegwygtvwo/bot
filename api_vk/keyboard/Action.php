<?php

namespace vk\keyboard;

class Action
{
    public
        $type = 'text',
        $payload = null,
        $label = null;
    function __construct($payload, $label)
    {
        $this->payload = $payload;
        $this->label = $label;
    }
}