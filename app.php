<?php

//config & constants
require_once "config/php_config.php";
require_once "config/constants.php";
require_once "config/error_const.php";

//database
require_once ROOT."/src/database/Database.php";
require_once ROOT."/src/database/State.php";
require_once ROOT."/src/database/Token.php";
require_once ROOT."/src/database/Meta.php";
require_once ROOT."/src/database/Owner.php";
require_once ROOT."/src/database/Group.php";
require_once ROOT."/src/database/Type.php";
require_once ROOT."/src/database/Bot.php";
require_once ROOT."/src/database/CacheMenu.php";
require_once ROOT."/src/database/CacheCard.php";
require_once ROOT."/src/database/Common.php";
require_once ROOT."/src/database/Cart.php";

//lib
require_once ROOT."/lib/logger.php";
require_once ROOT."/lib/Request.php";

//translator
require_once ROOT."/src/translator/Translator.php";
require_once ROOT."/src/translator/TranslatorMobidel.php";
require_once ROOT."/src/translator/ComponentTranslator.php";

//executor
require_once ROOT."/src/executor/Reader.php";
require_once ROOT."/src/executor/Executor.php";

//functions
require_once ROOT."/src/functions/FunctionsInterface.php";
require_once ROOT."/src/functions/MobidelFunctions.php";

//crm\mobidel
require_once ROOT."/src/crm/mobidel/Parse_menu.php";
//crm\common
require_once ROOT."/src/crm/common/ProductCard.php";

//api_crm
//api_crm\mobidel
require_once ROOT."/api_crm/mobidel/Shop.php";
require_once ROOT."/api_crm/mobidel/Client.php";

//vk
require_once ROOT."/api_vk/Output.php";
require_once ROOT."/api_vk/Vk_api.php";
require_once ROOT."/api_vk/Messages.php";
require_once ROOT."/api_vk/Photos.php";
require_once ROOT."/api_vk/Users.php";
// vk\keyboard
require_once ROOT."/api_vk/keyboard/Keyboard.php";
require_once ROOT."/api_vk/keyboard/Button.php";
require_once ROOT."/api_vk/keyboard/Action.php";
// vk\keyboard\
require_once ROOT."/api_vk/keyboard/keyboard_patterns/MobidelPattern.php";
